﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="BecomeAgency.aspx.cs" Inherits="POLY_AIS.BecomeAgency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
        <div class="row">
            <div class="col-md-12">
                <div class="featured container">
                    <div class="content-wrapper container">
                        <h1>Agency Application</h1>
                        <br>
                        <br>
                        <p>
                            In order for an organization to be considered as a POLY partner, the organization must complete this  application form.
                        Additionally, the organization must have applicable business licenses and/or  permits to conduct business for the location.
                        If your organization has more than one location and wants to have separate accounts, you must apply  for each location
                        separately.  For each location, there must be one unique email address as an  administrator to manage each account.
                        </p>
                        <p>
                            If your organization has more than one location, please designate your Main Branch or Headquarters as the "Company Name"
                        and then use the "Add Branch" function to add branches. The Main Branch must designate a user as an administrator to
                        manage the account.
                        </p>
                        <p>
                            Upon submission of this application, please email as attachments the copies of applicable licenses  and/or permits,
                        if any, to hq@polylanguages.edu.  The license(s) or permit(s) can be in the language  provided by the governing entities
                        applicable to the organization where the business is practiced. If your application is accepted, we will send you the
                        Agency Agreement, which must be signed to  complete the acceptance of your organization as our partner.
                        Please contact hq@polylanguages.edu for any questions, and we appreciate very much for your interest in working with
                        POLY Languages Institute.
                        </p>
                    </div>
                </div>

                <section class="main-content form-horizontal">

                    <div class="sub-title">
                        <h2><span class="fa fa-info-circle"></span>Company Information</h2>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label label-required text-primary" for="CompanyName"><span class="text-required">*</span> Company Name</label>
                        <div class="col-xs-8">
                            <input class="form-control" data-val="true" data-val-length="The field Company Name must be a string with a maximum length of 100." data-val-length-max="100" data-val-required="The Company Name field is required." id="CompanyName" name="CompanyName" type="text" value="" runat="server">
                        </div>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label label-required text-primary" for="Address"><span class="text-required">*</span> Address</label>
                        <div class="col-xs-8">
                            <input class="form-control" data-val="true" data-val-length="The field Address must be a string with a maximum length of 100." data-val-length-max="100" data-val-required="The Address field is required." id="Address" name="Address" type="text" value="" runat="server">
                        </div>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label text-primary" for="City">City</label>
                        <div class="col-xs-8">
                            <input class="form-control" data-val="true" data-val-length="The field City must be a string with a maximum length of 50." data-val-length-max="50" id="City" name="City" type="text" value="" runat="server">
                        </div>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label text-primary" for="PostalCode">Postal Code</label>
                        <div class="col-xs-3">
                            <input class="form-control" data-val="true" data-val-length="The field Postal Code must be a string with a maximum length of 20." data-val-length-max="20" id="PostalCode" name="PostalCode" type="text" value="" runat="server">
                        </div>
                        <label class="col-xs-2 control-label text-primary" for="Province">Province / Territory</label>
                        <div class="col-xs-3">
                            <input class="form-control" data-val="true" data-val-length="The field Province / Territory must be a string with a maximum length of 30." data-val-length-max="30" id="Province" name="Province" type="text" value="" runat="server">
                        </div>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label text-primary" for="CountryCode">Country</label>
                        <div class="col-xs-3">
                            <asp:DropDownList runat="server" ID="CountryCode" DataSourceID="SDS_Coutry" AppendDataBoundItems="true"
                                DataTextField="Country" DataValueField="CountryCode" required>
                                <asp:ListItem Text="-Select Country of Birth-" Value="0" />
                            </asp:DropDownList>
                            <%--<select autocomplete="off" data-val="true" data-val-length="The field Country must be a string with a maximum length of 50." data-val-length-max="50" id="CountryCode" name="CountryCode">
                                            <option value=""></option>
                                            <option value="AFG">Afghanistan</option>
                                            <option value="ALG">Algeria</option>
                                            <option value="ARG">Argentina</option>
                                            <option value="ARM">Armenia</option>
                                            <option value="AUS">Australia</option>
                                            <option value="AUT">Austria</option>
                                            <option value="AZE">Azerbaijan</option>
                                            <option value="NAG">Bangladesh</option>
                                            <option value="BLR">Belarus</option>
                                            <option value="BEL">Belgium</option>
                                            <option value="BIZ">Belize</option>
                                            <option value="BEN">Benin</option>
                                            <option value="BOL">Bolivia</option>
                                            <option value="BIH">Bosnia and Herzegovina</option>
                                            <option value="BRA">Brazil</option>
                                            <option value="IVB">British Virgin Islands*</option>
                                            <option value="BRU">Brunei</option>
                                            <option value="BUL">Bulgaria</option>
                                            <option value="CAM">Cambodia</option>
                                            <option value="CMR">Cameroon</option>
                                            <option value="CAN">Canada</option>
                                            <option value="CAF">Central African Republic</option>
                                            <option value="CHI">Chile</option>
                                            <option value="CHN">China</option>
                                            <option value="COL">Colombia</option>
                                            <option value="CON">Congo</option>
                                            <option value="CRC">Costa Rica</option>
                                            <option value="CIV">Côte d'Ivoire</option>
                                            <option value="CRO">Croatia</option>
                                            <option value="CUB">Cuba</option>
                                            <option value="CZE">Czech Republic</option>
                                            <option value="DEN">Denmark</option>
                                            <option value="DMA">Dominica</option>
                                            <option value="ECU">Ecuador</option>
                                            <option value="EGY">Egypt</option>
                                            <option value="ESA">El Salvador</option>
                                            <option value="EST">Estonia</option>
                                            <option value="ETH">Ethiopia</option>
                                            <option value="FIN">Finland</option>
                                            <option value="FRA">France</option>
                                            <option value="XPF">French Polynesia</option>
                                            <option value="GAB">Gabon</option>
                                            <option value="GEO">Georgia</option>
                                            <option value="GER">Germany</option>
                                            <option value="GHA">Ghana</option>
                                            <option value="GRE">Greece</option>
                                            <option value="GUA">Guatemala</option>
                                            <option value="HAI">Haiti</option>
                                            <option value="HON">Honduras</option>
                                            <option value="HKG">Hong Kong*</option>
                                            <option value="HUN">Hungary</option>
                                            <option value="ISL">Iceland</option>
                                            <option value="IND">India</option>
                                            <option value="INA">Indonesia</option>
                                            <option value="IRI">Iran</option>
                                            <option value="IRQ">Iraq</option>
                                            <option value="IRL">Ireland</option>
                                            <option value="ISR">Israel</option>
                                            <option value="ITA">Italy</option>
                                            <option value="JAM">Jamaica</option>
                                            <option value="JPN">Japan</option>
                                            <option value="JOR">Jordan</option>
                                            <option value="KAZ">Kazakhstan</option>
                                            <option value="KEN">Kenya</option>
                                            <option value="KOR">Korea, South</option>
                                            <option value="KOS">Kosovo</option>
                                            <option value="KUW">Kuwait</option>
                                            <option value="LAO">Laos</option>
                                            <option value="LAT">Latvia</option>
                                            <option value="LEB">Lebanon</option>
                                            <option value="LBA">Libya</option>
                                            <option value="LTU">Lithuania</option>
                                            <option value="MAC">Macau</option>
                                            <option value="MKD">Macedonia</option>
                                            <option value="MAS">Malaysia</option>
                                            <option value="MLI">Mali, Republic of</option>
                                            <option value="MEX">Mexico</option>
                                            <option value="MDA">Moldova</option>
                                            <option value="MON">Monaco</option>
                                            <option value="MGL">Mongolia</option>
                                            <option value="MTG">Montenegro</option>
                                            <option value="MAR">Morocco</option>
                                            <option value="NEP">Nepal</option>
                                            <option value="NED">Netherlands</option>
                                            <option value="NZL">New Zealand</option>
                                            <option value="NCA">Nicaragua</option>
                                            <option value="NIG">Niger</option>
                                            <option value="NGR">Nigeria</option>
                                            <option value="NOR">Norway</option>
                                            <option value="OMA">Oman</option>
                                            <option value="PAK">Pakistan</option>
                                            <option value="PAN">Panama</option>
                                            <option value="PAR">Paraguay</option>
                                            <option value="PER">Peru</option>
                                            <option value="PHI">Philippines</option>
                                            <option value="POL">Poland</option>
                                            <option value="POR">Portugal</option>
                                            <option value="PUR">Puerto Rico*</option>
                                            <option value="QAT">Qatar</option>
                                            <option value="ROM">Romania</option>
                                            <option value="RUS">Russia</option>
                                            <option value="KSA">Saudi Arabia</option>
                                            <option value="SEN">Senegal</option>
                                            <option value="SRB">Serbia</option>
                                            <option value="SIN">Singapore</option>
                                            <option value="SVK">Slovakia</option>
                                            <option value="SLO">Slovenia</option>
                                            <option value="RSA">South Africa</option>
                                            <option value="ESP">Spain</option>
                                            <option value="SUD">Sudan</option>
                                            <option value="SWZ">Swaziland</option>
                                            <option value="SWE">Sweden</option>
                                            <option value="SUI">Switzerland</option>
                                            <option value="SYR">Syria</option>
                                            <option value="TPE">Taiwan</option>
                                            <option value="TJK">Tajikistan</option>
                                            <option value="THA">Thailand</option>
                                            <option value="BAH">The Bahamas</option>
                                            <option value="TGA">Tonga</option>
                                            <option value="TUN">Tunisia</option>
                                            <option value="TUR">Turkey</option>
                                            <option value="TKM">Turkmenistan</option>
                                            <option value="UGA">Uganda</option>
                                            <option value="UKR">Ukraine</option>
                                            <option value="UAE">United Arab Emirates</option>
                                            <option value="GBR">United Kingdom (Great Britain)</option>
                                            <option value="USA">United States</option>
                                            <option value="URU">Uruguay</option>
                                            <option value="UZB">Uzbekistan</option>
                                            <option value="VEN">Venezuela</option>
                                            <option value="VIE">Vietnam</option>
                                            <option value="YEM">Yemen</option>
                                            <option value="YUG">Yugoslavia</option>
                                            <option value="ZAM">Zambia</option>
                                        </select>--%>
                            <input id="Country" name="Country" type="hidden" runat="server" value="">
                        </div>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label label-required text-primary" for="Tel"><span class="text-required">*</span> Tel</label>
                        <div class="col-xs-3">
                            <input class="form-control" data-val="true" data-val-length="The field Tel must be a string with a maximum length of 20." data-val-length-max="20" data-val-required="The Tel field is required." id="Tel" name="Tel" type="text" value="" runat="server">
                        </div>
                        <label class="col-xs-2 control-label text-primary" for="Fax">Fax</label>
                        <div class="col-xs-3">
                            <input class="form-control" data-val="true" data-val-length="The field Fax must be a string with a maximum length of 20." data-val-length-max="20" id="Fax" name="Fax" type="text" value="" runat="server">
                        </div>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label label-required text-primary" for="WebAddress"><span class="text-required">*</span> Website</label>
                        <div class="col-xs-8">
                            <input class="form-control" data-val="true" data-val-length="The field Website must be a string with a maximum length of 150." data-val-length-max="150" data-val-required="The Website field is required." id="WebAddress" name="WebAddress" type="text" value="" runat="server">
                        </div>
                    </div>
                    <div class="sub-title">
                        <h2><span class="fa fa-info-circle"></span>Branch Information</h2>
                    </div>
                    <div id="branch1" class="branch">
                        <div class="form-group form-field">
                            <div class="col-xs-2 text-primary text-branch">
                                <spa class="fa fa-sitemap"></spa>
                                Branch 1
                            </div>
                            <label class="col-xs-2 control-label text-primary" for="CompanyName">Company Name</label>
                            <div class="col-xs-7">
                                <input class="form-control" id="B1CompanyName" name="B1CompanyName" type="text" value="" runat="server">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <label class="col-xs-2 control-label text-primary" for="Address">Address</label>
                            <div class="col-xs-5">
                                <input class="form-control" id="B1Address" name="B1Address" type="text" value="" runat="server">
                            </div>
                            <label class="col-xs-1 control-label text-primary" for="City">City</label>
                            <div class="col-xs-3">
                                <input class="form-control" id="B1City" name="B1City" type="text" value="" runat="server">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <label class="col-xs-2 control-label text-primary" for="PostalCode">Postal Code</label>
                            <div class="col-xs-2">
                                <input class="form-control" id="B1PostalCode" name="B1PostalCode" type="text" value="" runat="server">
                            </div>
                            <div class="col-xs-1 control-label text-primary text-bold">
                                Province
                            </div>
                            <div class="col-xs-2">
                                <input class="form-control" id="B1Province" name="B1Province" type="text" value="" runat="server">
                            </div>
                            <label class="col-xs-1 control-label text-primary" for="CountryCode">Country</label>
                            <div class="col-xs-3">
                                 <asp:DropDownList runat="server" ID="B1CountryCode" DataSourceID="SDS_Coutry" AppendDataBoundItems="true"
                                DataTextField="Country" DataValueField="CountryCode"  >
                                <asp:ListItem Text="-Select Country of Birth-" Value="0" />
                            </asp:DropDownList>

                                <%--<select autocomplete="off" id="B1CountryCode" name="B1CountryCode">
                                    <option value=""></option>
                                    <option value="AFG">Afghanistan</option>
                                    <option value="ALG">Algeria</option>
                                    <option value="ARG">Argentina</option>
                                    <option value="ARM">Armenia</option>
                                    <option value="AUS">Australia</option>
                                    <option value="AUT">Austria</option>
                                    <option value="AZE">Azerbaijan</option>
                                    <option value="NAG">Bangladesh</option>
                                    <option value="BLR">Belarus</option>
                                    <option value="BEL">Belgium</option>
                                    <option value="BIZ">Belize</option>
                                    <option value="BEN">Benin</option>
                                    <option value="BOL">Bolivia</option>
                                    <option value="BIH">Bosnia and Herzegovina</option>
                                    <option value="BRA">Brazil</option>
                                    <option value="IVB">British Virgin Islands*</option>
                                    <option value="BRU">Brunei</option>
                                    <option value="BUL">Bulgaria</option>
                                    <option value="CAM">Cambodia</option>
                                    <option value="CMR">Cameroon</option>
                                    <option value="CAN">Canada</option>
                                    <option value="CAF">Central African Republic</option>
                                    <option value="CHI">Chile</option>
                                    <option value="CHN">China</option>
                                    <option value="COL">Colombia</option>
                                    <option value="CON">Congo</option>
                                    <option value="CRC">Costa Rica</option>
                                    <option value="CIV">Côte d'Ivoire</option>
                                    <option value="CRO">Croatia</option>
                                    <option value="CUB">Cuba</option>
                                    <option value="CZE">Czech Republic</option>
                                    <option value="DEN">Denmark</option>
                                    <option value="DMA">Dominica</option>
                                    <option value="ECU">Ecuador</option>
                                    <option value="EGY">Egypt</option>
                                    <option value="ESA">El Salvador</option>
                                    <option value="EST">Estonia</option>
                                    <option value="ETH">Ethiopia</option>
                                    <option value="FIN">Finland</option>
                                    <option value="FRA">France</option>
                                    <option value="XPF">French Polynesia</option>
                                    <option value="GAB">Gabon</option>
                                    <option value="GEO">Georgia</option>
                                    <option value="GER">Germany</option>
                                    <option value="GHA">Ghana</option>
                                    <option value="GRE">Greece</option>
                                    <option value="GUA">Guatemala</option>
                                    <option value="HAI">Haiti</option>
                                    <option value="HON">Honduras</option>
                                    <option value="HKG">Hong Kong*</option>
                                    <option value="HUN">Hungary</option>
                                    <option value="ISL">Iceland</option>
                                    <option value="IND">India</option>
                                    <option value="INA">Indonesia</option>
                                    <option value="IRI">Iran</option>
                                    <option value="IRQ">Iraq</option>
                                    <option value="IRL">Ireland</option>
                                    <option value="ISR">Israel</option>
                                    <option value="ITA">Italy</option>
                                    <option value="JAM">Jamaica</option>
                                    <option value="JPN">Japan</option>
                                    <option value="JOR">Jordan</option>
                                    <option value="KAZ">Kazakhstan</option>
                                    <option value="KEN">Kenya</option>
                                    <option value="KOR">Korea, South</option>
                                    <option value="KOS">Kosovo</option>
                                    <option value="KUW">Kuwait</option>
                                    <option value="LAO">Laos</option>
                                    <option value="LAT">Latvia</option>
                                    <option value="LEB">Lebanon</option>
                                    <option value="LBA">Libya</option>
                                    <option value="LTU">Lithuania</option>
                                    <option value="MAC">Macau</option>
                                    <option value="MKD">Macedonia</option>
                                    <option value="MAS">Malaysia</option>
                                    <option value="MLI">Mali, Republic of</option>
                                    <option value="MEX">Mexico</option>
                                    <option value="MDA">Moldova</option>
                                    <option value="MON">Monaco</option>
                                    <option value="MGL">Mongolia</option>
                                    <option value="MTG">Montenegro</option>
                                    <option value="MAR">Morocco</option>
                                    <option value="NEP">Nepal</option>
                                    <option value="NED">Netherlands</option>
                                    <option value="NZL">New Zealand</option>
                                    <option value="NCA">Nicaragua</option>
                                    <option value="NIG">Niger</option>
                                    <option value="NGR">Nigeria</option>
                                    <option value="NOR">Norway</option>
                                    <option value="OMA">Oman</option>
                                    <option value="PAK">Pakistan</option>
                                    <option value="PAN">Panama</option>
                                    <option value="PAR">Paraguay</option>
                                    <option value="PER">Peru</option>
                                    <option value="PHI">Philippines</option>
                                    <option value="POL">Poland</option>
                                    <option value="POR">Portugal</option>
                                    <option value="PUR">Puerto Rico*</option>
                                    <option value="QAT">Qatar</option>
                                    <option value="ROM">Romania</option>
                                    <option value="RUS">Russia</option>
                                    <option value="KSA">Saudi Arabia</option>
                                    <option value="SEN">Senegal</option>
                                    <option value="SRB">Serbia</option>
                                    <option value="SIN">Singapore</option>
                                    <option value="SVK">Slovakia</option>
                                    <option value="SLO">Slovenia</option>
                                    <option value="RSA">South Africa</option>
                                    <option value="ESP">Spain</option>
                                    <option value="SUD">Sudan</option>
                                    <option value="SWZ">Swaziland</option>
                                    <option value="SWE">Sweden</option>
                                    <option value="SUI">Switzerland</option>
                                    <option value="SYR">Syria</option>
                                    <option value="TPE">Taiwan</option>
                                    <option value="TJK">Tajikistan</option>
                                    <option value="THA">Thailand</option>
                                    <option value="BAH">The Bahamas</option>
                                    <option value="TGA">Tonga</option>
                                    <option value="TUN">Tunisia</option>
                                    <option value="TUR">Turkey</option>
                                    <option value="TKM">Turkmenistan</option>
                                    <option value="UGA">Uganda</option>
                                    <option value="UKR">Ukraine</option>
                                    <option value="UAE">United Arab Emirates</option>
                                    <option value="GBR">United Kingdom (Great Britain)</option>
                                    <option value="USA">United States</option>
                                    <option value="URU">Uruguay</option>
                                    <option value="UZB">Uzbekistan</option>
                                    <option value="VEN">Venezuela</option>
                                    <option value="VIE">Vietnam</option>
                                    <option value="YEM">Yemen</option>
                                    <option value="YUG">Yugoslavia</option>
                                    <option value="ZAM">Zambia</option>
                                </select>--%>
                                <input id="B1Country" name="B1Country" type="hidden" value="">
                            </div>
                        </div>
                    </div>
                    <div id="branch2" class="branch">
                        <div class="form-group form-field">
                            <div class="col-xs-2 text-primary text-branch">
                                <spa class="fa fa-sitemap"></spa>
                                Branch 2
                            </div>
                            <label class="col-xs-2 control-label text-primary" for="CompanyName">Company Name</label>
                            <div class="col-xs-7">
                                <input class="form-control" id="B2CompanyName" name="B2CompanyName" type="text" value="" runat="server">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <label class="col-xs-2 control-label text-primary" for="Address">Address</label>
                            <div class="col-xs-5">
                                <input class="form-control" id="B2Address" name="B2Address" type="text" value="" runat="server">
                            </div>
                            <label class="col-xs-1 control-label text-primary" for="City">City</label>
                            <div class="col-xs-3">
                                <input class="form-control" id="B2City" name="B2City" type="text" value="" runat="server">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <label class="col-xs-2 control-label text-primary" for="PostalCode">Postal Code</label>
                            <div class="col-xs-2">
                                <input class="form-control" id="B2PostalCode" name="B2PostalCode" type="text" value="" runat="server">
                            </div>
                            <div class="col-xs-1 control-label text-primary text-bold">
                                Province
                            </div>
                            <div class="col-xs-2">
                                <input class="form-control" id="B2Province" name="Province" type="text" value="" runat="server">
                            </div>
                            <label class="col-xs-1 control-label text-primary" for="CountryCode">Country</label>
                            <div class="col-xs-3">
                                 <asp:DropDownList runat="server" ID="B2CountryCode" DataSourceID="SDS_Coutry" AppendDataBoundItems="true"
                                DataTextField="Country" DataValueField="CountryCode"  >
                                <asp:ListItem Text="-Select Country of Birth-" Value="0" />
                            </asp:DropDownList>
                                <%--<select autocomplete="off" id="B2CountryCode" name="B2CountryCode">
                                    <option value=""></option>
                                    <option value="AFG">Afghanistan</option>
                                    <option value="ALG">Algeria</option>
                                    <option value="ARG">Argentina</option>
                                    <option value="ARM">Armenia</option>
                                    <option value="AUS">Australia</option>
                                    <option value="AUT">Austria</option>
                                    <option value="AZE">Azerbaijan</option>
                                    <option value="NAG">Bangladesh</option>
                                    <option value="BLR">Belarus</option>
                                    <option value="BEL">Belgium</option>
                                    <option value="BIZ">Belize</option>
                                    <option value="BEN">Benin</option>
                                    <option value="BOL">Bolivia</option>
                                    <option value="BIH">Bosnia and Herzegovina</option>
                                    <option value="BRA">Brazil</option>
                                    <option value="IVB">British Virgin Islands*</option>
                                    <option value="BRU">Brunei</option>
                                    <option value="BUL">Bulgaria</option>
                                    <option value="CAM">Cambodia</option>
                                    <option value="CMR">Cameroon</option>
                                    <option value="CAN">Canada</option>
                                    <option value="CAF">Central African Republic</option>
                                    <option value="CHI">Chile</option>
                                    <option value="CHN">China</option>
                                    <option value="COL">Colombia</option>
                                    <option value="CON">Congo</option>
                                    <option value="CRC">Costa Rica</option>
                                    <option value="CIV">Côte d'Ivoire</option>
                                    <option value="CRO">Croatia</option>
                                    <option value="CUB">Cuba</option>
                                    <option value="CZE">Czech Republic</option>
                                    <option value="DEN">Denmark</option>
                                    <option value="DMA">Dominica</option>
                                    <option value="ECU">Ecuador</option>
                                    <option value="EGY">Egypt</option>
                                    <option value="ESA">El Salvador</option>
                                    <option value="EST">Estonia</option>
                                    <option value="ETH">Ethiopia</option>
                                    <option value="FIN">Finland</option>
                                    <option value="FRA">France</option>
                                    <option value="XPF">French Polynesia</option>
                                    <option value="GAB">Gabon</option>
                                    <option value="GEO">Georgia</option>
                                    <option value="GER">Germany</option>
                                    <option value="GHA">Ghana</option>
                                    <option value="GRE">Greece</option>
                                    <option value="GUA">Guatemala</option>
                                    <option value="HAI">Haiti</option>
                                    <option value="HON">Honduras</option>
                                    <option value="HKG">Hong Kong*</option>
                                    <option value="HUN">Hungary</option>
                                    <option value="ISL">Iceland</option>
                                    <option value="IND">India</option>
                                    <option value="INA">Indonesia</option>
                                    <option value="IRI">Iran</option>
                                    <option value="IRQ">Iraq</option>
                                    <option value="IRL">Ireland</option>
                                    <option value="ISR">Israel</option>
                                    <option value="ITA">Italy</option>
                                    <option value="JAM">Jamaica</option>
                                    <option value="JPN">Japan</option>
                                    <option value="JOR">Jordan</option>
                                    <option value="KAZ">Kazakhstan</option>
                                    <option value="KEN">Kenya</option>
                                    <option value="KOR">Korea, South</option>
                                    <option value="KOS">Kosovo</option>
                                    <option value="KUW">Kuwait</option>
                                    <option value="LAO">Laos</option>
                                    <option value="LAT">Latvia</option>
                                    <option value="LEB">Lebanon</option>
                                    <option value="LBA">Libya</option>
                                    <option value="LTU">Lithuania</option>
                                    <option value="MAC">Macau</option>
                                    <option value="MKD">Macedonia</option>
                                    <option value="MAS">Malaysia</option>
                                    <option value="MLI">Mali, Republic of</option>
                                    <option value="MEX">Mexico</option>
                                    <option value="MDA">Moldova</option>
                                    <option value="MON">Monaco</option>
                                    <option value="MGL">Mongolia</option>
                                    <option value="MTG">Montenegro</option>
                                    <option value="MAR">Morocco</option>
                                    <option value="NEP">Nepal</option>
                                    <option value="NED">Netherlands</option>
                                    <option value="NZL">New Zealand</option>
                                    <option value="NCA">Nicaragua</option>
                                    <option value="NIG">Niger</option>
                                    <option value="NGR">Nigeria</option>
                                    <option value="NOR">Norway</option>
                                    <option value="OMA">Oman</option>
                                    <option value="PAK">Pakistan</option>
                                    <option value="PAN">Panama</option>
                                    <option value="PAR">Paraguay</option>
                                    <option value="PER">Peru</option>
                                    <option value="PHI">Philippines</option>
                                    <option value="POL">Poland</option>
                                    <option value="POR">Portugal</option>
                                    <option value="PUR">Puerto Rico*</option>
                                    <option value="QAT">Qatar</option>
                                    <option value="ROM">Romania</option>
                                    <option value="RUS">Russia</option>
                                    <option value="KSA">Saudi Arabia</option>
                                    <option value="SEN">Senegal</option>
                                    <option value="SRB">Serbia</option>
                                    <option value="SIN">Singapore</option>
                                    <option value="SVK">Slovakia</option>
                                    <option value="SLO">Slovenia</option>
                                    <option value="RSA">South Africa</option>
                                    <option value="ESP">Spain</option>
                                    <option value="SUD">Sudan</option>
                                    <option value="SWZ">Swaziland</option>
                                    <option value="SWE">Sweden</option>
                                    <option value="SUI">Switzerland</option>
                                    <option value="SYR">Syria</option>
                                    <option value="TPE">Taiwan</option>
                                    <option value="TJK">Tajikistan</option>
                                    <option value="THA">Thailand</option>
                                    <option value="BAH">The Bahamas</option>
                                    <option value="TGA">Tonga</option>
                                    <option value="TUN">Tunisia</option>
                                    <option value="TUR">Turkey</option>
                                    <option value="TKM">Turkmenistan</option>
                                    <option value="UGA">Uganda</option>
                                    <option value="UKR">Ukraine</option>
                                    <option value="UAE">United Arab Emirates</option>
                                    <option value="GBR">United Kingdom (Great Britain)</option>
                                    <option value="USA">United States</option>
                                    <option value="URU">Uruguay</option>
                                    <option value="UZB">Uzbekistan</option>
                                    <option value="VEN">Venezuela</option>
                                    <option value="VIE">Vietnam</option>
                                    <option value="YEM">Yemen</option>
                                    <option value="YUG">Yugoslavia</option>
                                    <option value="ZAM">Zambia</option>
                                </select>--%>
                                <input id="B2Country" name="B2Country" type="hidden" value="" runat="server">
                            </div>
                        </div>
                    </div>
                    <div id="branch3" class="branch">
                        <div class="form-group form-field">
                            <div class="col-xs-2 text-primary text-branch">
                                <spa class="fa fa-sitemap"></spa>
                                Branch 3
                            </div>
                            <label class="col-xs-2 control-label text-primary" for="CompanyName">Company Name</label>
                            <div class="col-xs-7">
                                <input class="form-control" id="B3CompanyName" name="B3CompanyName" type="text" value="" runat="server">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <label class="col-xs-2 control-label text-primary" for="Address">Address</label>
                            <div class="col-xs-5">
                                <input class="form-control" id="B3Address" name="B3Address" type="text" value="" runat="server">
                            </div>
                            <label class="col-xs-1 control-label text-primary" for="City">City</label>
                            <div class="col-xs-3">
                                <input class="form-control" id="B3City" name="B3City" type="text" value="" runat="server">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <label class="col-xs-2 control-label text-primary" for="PostalCode">Postal Code</label>
                            <div class="col-xs-2">
                                <input class="form-control" id="B3PostalCode" name="B3PostalCode" type="text" value="" runat="server">
                            </div>
                            <div class="col-xs-1 control-label text-primary text-bold">
                                Province
                            </div>
                            <div class="col-xs-2">
                                <input class="form-control" id="B3Province" name="B3Province" type="text" value="" runat="server">
                            </div>
                            <label class="col-xs-1 control-label text-primary" for="CountryCode">Country</label>
                            <div class="col-xs-3">
                                 <asp:DropDownList runat="server" ID="B3CountryCode" DataSourceID="SDS_Coutry" AppendDataBoundItems="true"
                                DataTextField="Country" DataValueField="CountryCode"  >
                                <asp:ListItem Text="-Select Country of Birth-" Value="0" />
                            </asp:DropDownList>
                                <%--<select autocomplete="off" id="B3CountryCode" name="B3CountryCode">
                                    <option value=""></option>
                                    <option value="AFG">Afghanistan</option>
                                    <option value="ALG">Algeria</option>
                                    <option value="ARG">Argentina</option>
                                    <option value="ARM">Armenia</option>
                                    <option value="AUS">Australia</option>
                                    <option value="AUT">Austria</option>
                                    <option value="AZE">Azerbaijan</option>
                                    <option value="NAG">Bangladesh</option>
                                    <option value="BLR">Belarus</option>
                                    <option value="BEL">Belgium</option>
                                    <option value="BIZ">Belize</option>
                                    <option value="BEN">Benin</option>
                                    <option value="BOL">Bolivia</option>
                                    <option value="BIH">Bosnia and Herzegovina</option>
                                    <option value="BRA">Brazil</option>
                                    <option value="IVB">British Virgin Islands*</option>
                                    <option value="BRU">Brunei</option>
                                    <option value="BUL">Bulgaria</option>
                                    <option value="CAM">Cambodia</option>
                                    <option value="CMR">Cameroon</option>
                                    <option value="CAN">Canada</option>
                                    <option value="CAF">Central African Republic</option>
                                    <option value="CHI">Chile</option>
                                    <option value="CHN">China</option>
                                    <option value="COL">Colombia</option>
                                    <option value="CON">Congo</option>
                                    <option value="CRC">Costa Rica</option>
                                    <option value="CIV">Côte d'Ivoire</option>
                                    <option value="CRO">Croatia</option>
                                    <option value="CUB">Cuba</option>
                                    <option value="CZE">Czech Republic</option>
                                    <option value="DEN">Denmark</option>
                                    <option value="DMA">Dominica</option>
                                    <option value="ECU">Ecuador</option>
                                    <option value="EGY">Egypt</option>
                                    <option value="ESA">El Salvador</option>
                                    <option value="EST">Estonia</option>
                                    <option value="ETH">Ethiopia</option>
                                    <option value="FIN">Finland</option>
                                    <option value="FRA">France</option>
                                    <option value="XPF">French Polynesia</option>
                                    <option value="GAB">Gabon</option>
                                    <option value="GEO">Georgia</option>
                                    <option value="GER">Germany</option>
                                    <option value="GHA">Ghana</option>
                                    <option value="GRE">Greece</option>
                                    <option value="GUA">Guatemala</option>
                                    <option value="HAI">Haiti</option>
                                    <option value="HON">Honduras</option>
                                    <option value="HKG">Hong Kong*</option>
                                    <option value="HUN">Hungary</option>
                                    <option value="ISL">Iceland</option>
                                    <option value="IND">India</option>
                                    <option value="INA">Indonesia</option>
                                    <option value="IRI">Iran</option>
                                    <option value="IRQ">Iraq</option>
                                    <option value="IRL">Ireland</option>
                                    <option value="ISR">Israel</option>
                                    <option value="ITA">Italy</option>
                                    <option value="JAM">Jamaica</option>
                                    <option value="JPN">Japan</option>
                                    <option value="JOR">Jordan</option>
                                    <option value="KAZ">Kazakhstan</option>
                                    <option value="KEN">Kenya</option>
                                    <option value="KOR">Korea, South</option>
                                    <option value="KOS">Kosovo</option>
                                    <option value="KUW">Kuwait</option>
                                    <option value="LAO">Laos</option>
                                    <option value="LAT">Latvia</option>
                                    <option value="LEB">Lebanon</option>
                                    <option value="LBA">Libya</option>
                                    <option value="LTU">Lithuania</option>
                                    <option value="MAC">Macau</option>
                                    <option value="MKD">Macedonia</option>
                                    <option value="MAS">Malaysia</option>
                                    <option value="MLI">Mali, Republic of</option>
                                    <option value="MEX">Mexico</option>
                                    <option value="MDA">Moldova</option>
                                    <option value="MON">Monaco</option>
                                    <option value="MGL">Mongolia</option>
                                    <option value="MTG">Montenegro</option>
                                    <option value="MAR">Morocco</option>
                                    <option value="NEP">Nepal</option>
                                    <option value="NED">Netherlands</option>
                                    <option value="NZL">New Zealand</option>
                                    <option value="NCA">Nicaragua</option>
                                    <option value="NIG">Niger</option>
                                    <option value="NGR">Nigeria</option>
                                    <option value="NOR">Norway</option>
                                    <option value="OMA">Oman</option>
                                    <option value="PAK">Pakistan</option>
                                    <option value="PAN">Panama</option>
                                    <option value="PAR">Paraguay</option>
                                    <option value="PER">Peru</option>
                                    <option value="PHI">Philippines</option>
                                    <option value="POL">Poland</option>
                                    <option value="POR">Portugal</option>
                                    <option value="PUR">Puerto Rico*</option>
                                    <option value="QAT">Qatar</option>
                                    <option value="ROM">Romania</option>
                                    <option value="RUS">Russia</option>
                                    <option value="KSA">Saudi Arabia</option>
                                    <option value="SEN">Senegal</option>
                                    <option value="SRB">Serbia</option>
                                    <option value="SIN">Singapore</option>
                                    <option value="SVK">Slovakia</option>
                                    <option value="SLO">Slovenia</option>
                                    <option value="RSA">South Africa</option>
                                    <option value="ESP">Spain</option>
                                    <option value="SUD">Sudan</option>
                                    <option value="SWZ">Swaziland</option>
                                    <option value="SWE">Sweden</option>
                                    <option value="SUI">Switzerland</option>
                                    <option value="SYR">Syria</option>
                                    <option value="TPE">Taiwan</option>
                                    <option value="TJK">Tajikistan</option>
                                    <option value="THA">Thailand</option>
                                    <option value="BAH">The Bahamas</option>
                                    <option value="TGA">Tonga</option>
                                    <option value="TUN">Tunisia</option>
                                    <option value="TUR">Turkey</option>
                                    <option value="TKM">Turkmenistan</option>
                                    <option value="UGA">Uganda</option>
                                    <option value="UKR">Ukraine</option>
                                    <option value="UAE">United Arab Emirates</option>
                                    <option value="GBR">United Kingdom (Great Britain)</option>
                                    <option value="USA">United States</option>
                                    <option value="URU">Uruguay</option>
                                    <option value="UZB">Uzbekistan</option>
                                    <option value="VEN">Venezuela</option>
                                    <option value="VIE">Vietnam</option>
                                    <option value="YEM">Yemen</option>
                                    <option value="YUG">Yugoslavia</option>
                                    <option value="ZAM">Zambia</option>
                                </select>--%>
                                <input id="B3Country" name="B3Country" type="hidden" value="" runat="server">
                            </div>
                        </div>
                    </div>
                    <div class="sub-title">
                        <h2><span class="fa fa-info-circle"></span>Questionaire</h2>
                    </div>
                    <div class="questionaire">
                        <div class="form-group form-field">
                            <div class="col-xs-2 control-label text-primary">
                                1)
                            </div>
                            <div class="col-xs-9">
                                Does your agency possess all applicable permits and licenses to conduct business in the said premise stated herein?
                                <asp:RadioButtonList runat="server" ID="Questionaire_1" RepeatDirection="Horizontal" Width="20%" >
                                    <asp:ListItem Text="Yes" Value="yes" />
                                    <asp:ListItem Text="No" Value="no" />
                                </asp:RadioButtonList>
                               <%-- <input id="Questionaire_1" name="Questionaire1" type="radio" value="yes"><label for="Questionaire_1"><span><span></span></span>Yes</label>
                                <input id="Questionaire_2" name="Questionaire1" type="radio" value="no"><label for="Questionaire_2"><span><span></span></span>No</label>--%>
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <div class="col-xs-2 control-label text-primary">
                                2)
                            </div>
                            <div class="col-xs-5">
                                Please, indicate the most common age group of your clients.
                            </div>
                            <div class="col-xs-1">
                                <input class="form-control" id="Questionaire2" name="Questionaire2" type="text" value="" runat="server">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <div class="col-xs-2 control-label text-primary">
                                3)
                            </div>
                            <div class="col-xs-7">
                                On the average, how many students do you anticipate to recruit for POLY every two months?
                            </div>
                            <div class="col-xs-1">
                                <input class="form-control" id="Questionaire3" name="Questionaire3" type="text" value=""  runat="server">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <div class="col-xs-2 control-label text-primary">
                                4)
                            </div>
                            <div class="col-xs-9">
                                Briefly describe your organization such as the year established, the size (number of staff), the type of services provided, etc.
                            </div>
                            <div class="col-xs-2">
                            </div>
                            <div class="col-xs-8">
                                <textarea class="form-control form-textarea" cols="20" id="Questionaire4" name="Questionaire4" rows="3"  runat="server"></textarea>
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <div class="col-xs-2 control-label text-primary">
                                5)
                            </div>
                            <div class="col-xs-9">
                                What features are important to your agency when working with a language school?
                            </div>
                            <div class="col-xs-2">
                            </div>
                            <div class="col-xs-8">
                                <textarea class="form-control form-textarea" cols="20" id="Questionaire5" name="Questionaire5" rows="3"  runat="server"></textarea>
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <div class="col-xs-2 control-label text-primary">
                                6)
                            </div>
                            <div class="col-xs-9">
                                Please tell us at least three language schools in the U.S.  that you have agreements with.
                            </div>
                            <div class="col-xs-2">
                            </div>
                            <div class="col-xs-8">
                                <textarea class="form-control form-textarea" cols="20" id="Questionaire6" name="Questionaire6" rows="3"  runat="server"></textarea>
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <div class="col-xs-2 control-label text-primary">
                                7)
                            </div>
                            <div class="col-xs-9">
                                Please let us know how you heard about us and why you are choosing to work with us?
                            </div>
                            <div class="col-xs-2">
                            </div>
                            <div class="col-xs-8">
                                <textarea class="form-control form-textarea" cols="20" id="Questionaire7" name="Questionaire7" rows="3"  runat="server"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="sub-title">
                        <h2><span class="fa fa-info-circle"></span>Agent Information</h2>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label label-required text-primary" for="FirstName"><span class="text-required">*</span> First Name</label>
                        <div class="col-xs-3">
                            <input  runat="server" class="form-control" data-val="true" data-val-length="The field First Name must be a string with a maximum length of 20." data-val-length-max="20" data-val-required="The First Name field is required." id="FirstName" name="FirstName" type="text" value="">
                        </div>
                        <label class="col-xs-2 control-label label-required text-primary" for="LastName"><span class="text-required">*</span> Last Name</label>
                        <div class="col-xs-3">
                            <input  runat="server" class="form-control" data-val="true" data-val-length="The field Last Name must be a string with a maximum length of 20." data-val-length-max="20" data-val-required="The Last Name field is required." id="LastName" name="LastName" type="text" value="">
                        </div>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label label-required text-primary" for="PositionTitle"><span class="text-required">*</span> Position</label>
                        <div class="col-xs-8">
                            <input  runat="server" class="form-control" data-val="true" data-val-length="The field Position must be a string with a maximum length of 20." data-val-length-max="20" data-val-required="The Position field is required." id="PositionTitle" name="PositionTitle" type="text" value="">
                        </div>
                    </div>
                    <div class="form-group form-field">
                        <label class="col-xs-2 control-label label-required text-primary" for="Email"><span class="text-required">*</span> Email</label>
                        <div class="col-xs-8">
                            <input  runat="server" class="form-control" data-val="true" data-val-regex="That doesn't look like an email." data-val-regex-pattern="^([\w-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" data-val-required="The Email field is required." id="Email" name="Email" type="text" value="">
                        </div>
                    </div>
                    <div class="sub-title">
                        <h2><span class="fa fa-info-circle"></span>Agency Testament</h2>
                    </div>
                    <div class="form-group form-field">
                        <div class="col-xs-2">
                        </div>
                        <div class="col-xs-9">
                            <p>
                                I, on behalf of the company, by checking the box below, certify that the company is a bona fide agency
                                which assists and consults its clients to study English as a Second Language/Test Preparation programs
                                in the United States. The company has all applicable licenses and permits to conduct  business in the
                                said premises as submitted to the school. With due diligence, the company trains its  staff to accurately
                                inform its clients about the school’s program, policies and procedures. I understand  that the school
                                has the right to terminate this agreement at any time for any reason. Any dispute or  legal claim shall
                                be resolved under the U.S. law in the city of Los Angeles, California.
                                <input  runat="server" data-val="true" data-val-required="The AgencyTestament field is required." id="AgencyTestament" name="AgencyTestament" type="checkbox" value="true"><input name="AgencyTestament" type="hidden" value="false"  runat="server">
                                <label for="AgencyTestament"><span><span></span></span></label>
                            </p>
                        </div>
                    </div>
                    <div class="form-group form-field">
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8">
                        </div>
                        <div class="col-xs-3 text-right">
                            <asp:Button Text="Apply Now" CssClass="btn btn-primary" OnClick="btnApply_Click" ID="btnApply" runat="server" />
                          <%--  <button type="button" class="btn btn-primary" id="btnApplyNow">
                                <span class="fa fa-send"></span>Apply Now
                            </button>--%>
                        </div>
                    </div>

                    <button id="btnOpenModal" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" style="display: none;"></button>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" title="Close">
                                        <img src="/AIS/Content/images/icon-close-black.png"></button>
                                    <h4 class="modal-title">Agency Application</h4>
                                </div>
                                <div class="modal-body" id="detailsDiv">
                                    <table class="modal-table">
                                        <tbody>
                                            <tr class="noHoverBgColor">
                                                <td>
                                                    <span class="fa fa-exclamation-circle text-red"></span>
                                                </td>
                                                <td class="text-danger" id="modal-message"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer no-padding-right">
                                    <div class="col-xs-9 modal-footer-message text-left">
                                    </div>
                                    <div class="col-xs-3">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:SqlDataSource ID="SDS_Coutry" runat="server" ConnectionString="<%$ ConnectionStrings:HQ %>"
                        SelectCommandType="StoredProcedure" SelectCommand="sp_GetCountries">
                        <SelectParameters>
                            <asp:Parameter Name="Active" DefaultValue="1" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </section>
            </div>
        </div>
    </div>
</asp:Content>
