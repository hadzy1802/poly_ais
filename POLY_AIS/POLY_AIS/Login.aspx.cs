﻿using POLY_AIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class Login : System.Web.UI.Page
    {
        private string csHQ = WebConfigurationManager.ConnectionStrings["HQ"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    SqlCommand cmd = new SqlCommand("sp_Agents_Login", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Email", Username.Value);
                    cmd.Parameters.AddWithValue("@Password", Password.Value);
                    conn.Open();
                    //StoredProcedure storedProcedure = new StoredProcedure("sp_Agents_Login", DataService.GetInstance("Agencies"), "dbo");
                    //storedProcedure.Command.AddParameter("@Email", (object)email, DbType.String);
                    //storedProcedure.Command.AddParameter("@Password", (object)password, DbType.String);
                    //return storedProcedure;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            AgentInfoObject obj = new AgentInfoObject();
                            obj.AgencyID = dr["AgencyID"].ToString();
                            obj.CompanyName = dr["CompanyName"].ToString();
                            obj.AgentID = dr["AgentID"].ToString();
                            obj.FirstName = dr["FirstName"].ToString();
                            obj.LastName = dr["LastName"].ToString();
                            obj.PositionTitle = dr["PositionTitle"].ToString();
                            obj.Admin = Convert.ToBoolean(dr["Admin"].ToString());
                            if (dr["Commission"] != DBNull.Value)
                                obj.Commission = Convert.ToInt32(dr["Commission"].ToString());
                            if (dr["EmployeeID"] != DBNull.Value)
                                obj.EmployeeID = Convert.ToInt32(dr["EmployeeID"].ToString());
                            obj.Email = dr["Email"].ToString();
                            obj.Password = dr["Password"].ToString();
                            obj.AgencyType = dr["AgencyType"].ToString();
                            if (dr["CourtesyTitle"] != DBNull.Value)
                                obj.CourtesyTitle = dr["CourtesyTitle"].ToString();

                            this.Session["AgentInformation"] = obj;
                            this.Session["AgentName"] = dr["FirstName"];
                            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "window.top.location.href='MessageBoard.aspx'", true);
                        }
                        else
                        {
                            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('User not Found..');", true);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
                //Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('"  + ex.ToString() + "');", true);
            }

        }
    }
}