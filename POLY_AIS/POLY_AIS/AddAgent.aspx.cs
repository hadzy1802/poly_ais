﻿using POLY_AIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class AddAgent : System.Web.UI.Page
    {
        private string csHQ = WebConfigurationManager.ConnectionStrings["HQ"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string id = (Request.QueryString["id"]);
                if (id != null)
                {
                    AgentInfoObject agentInfoObject = ((AgencyInfoObject)this.Session["AgencyInformation"]).Agents.Where<AgentInfoObject>((Func<AgentInfoObject, bool>)(a => a.AgentID == id)).FirstOrDefault<AgentInfoObject>();
                    AgentInfoObject agentInfo = new AgentInfoObject();
                    ddlTitle.SelectedItem.Text = agentInfoObject.CourtesyTitle;
                    ///agentInfo.Courtesies = Common.GetCourtesy();
                    AgencyID.Value = agentInfoObject.AgencyID;
                    AgentID.Value = agentInfoObject.AgentID;
                    //CourtesyTitle.InnerText = agentInfoObject.CourtesyTitle;
                    FirstName.Value = agentInfoObject.FirstName;
                    LastName.Value = agentInfoObject.LastName;
                    PositionTitle.Value = agentInfoObject.PositionTitle;
                    Email.Value = agentInfoObject.Email;
                    Password.Value = agentInfoObject.Password;
                    btnAdd.Text = "Update";
                }
                
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    SqlCommand cmd = new SqlCommand("sp_Agent_IUD", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    AgentInfoObject agent = (AgentInfoObject)Session["AgentInformation"];
                    cmd.Parameters.AddWithValue("@AgencyID", agent.AgencyID);
                    cmd.Parameters.AddWithValue("@CourtesyTitle", ddlTitle.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@FirstName", FirstName.Value);
                    cmd.Parameters.AddWithValue("@LastName", LastName.Value);
                    cmd.Parameters.AddWithValue("@PositionTitle", PositionTitle.Value);
                    cmd.Parameters.AddWithValue("@Email", Email.Value);
                   
                    if (AgentID.Value == "")
                    {
                        cmd.Parameters.AddWithValue("@IUD", "I");
                        cmd.Parameters.AddWithValue("@Pwd", Password2.Value);
                    }
                    else
                    {
                        if (Password2.Value == "")
                        {
                            cmd.Parameters.AddWithValue("@Pwd", Password.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Pwd", Password2.Value);
                        }
                        cmd.Parameters.AddWithValue("@IUD", "U");
                        cmd.Parameters.AddWithValue("@AgentID", AgentID.Value);
                    }
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    if (AgentID.Value == ""){
                    SetNotification("It was successfully added.");
                }
                    else
                    {
                        SetNotification("It was successfully Updated.");
                    }
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "window.top.location.href='Profile.aspx'", true);
                }
            }
            catch (Exception ex)
            {

                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.ToString() + "');", true);
            }
        }
        protected  void SetNotification(string notification)
        {
            this.Response.Cookies.Add(new HttpCookie("AgenciesNotification")
            {
                Value = string.Format(notification),
                Path = "/"
            });
        }
    }
}