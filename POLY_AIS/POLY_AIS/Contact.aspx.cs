﻿using POLY_AIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class Contact : System.Web.UI.Page
    {
        private string csHQ = WebConfigurationManager.ConnectionStrings["HQ"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["AgentName"] == null)
                {
                    Response.Redirect("Index.aspx?doLogin=true");
                }
                AgentInfoObject agentInformation = (AgentInfoObject)Session["AgentInformation"];
                List<ContactDetailObject> contactDetailObjectList = new List<ContactDetailObject>();
                using (SqlConnection conn = new SqlConnection(csHQ))
                {

                    //AgencyContactObject agencyContactObject2 = new AgencyContactObject();

                    AgencyContactObject agencyContactObject1 = new AgencyContactObject();

                    SqlCommand storedProcedure = new SqlCommand("sp_AgencyContacts_S", conn);
                    storedProcedure.Parameters.AddWithValue("@AgencyID", agentInformation.AgencyID);
                    storedProcedure.Parameters.AddWithValue("@AgentID", agentInformation.AgentID);
                    storedProcedure.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    DataSet dass = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = storedProcedure;
                    da.Fill(dass);
                    DataTable dt = (DataTable)dass.Tables[0];
                    DataRow drr = dt.Rows[0];

                    AgencyContactObject agencyContactObject2 = new AgencyContactObject();
                    agencyContactObject2.ACID = (int)drr["ACID"];
                    agencyContactObject2.AgencyID = drr["AgencyID"].ToString();
                    agencyContactObject2.AgentID = drr["AgentID"].ToString();
                    agencyContactObject2.Alias = drr["Alias"].ToString();
                    agencyContactObject2.CompanyName = drr["CompanyName"].ToString();
                    conn.Close();
                    //==============================================================================================================//
                    if (agencyContactObject2 != null && agencyContactObject2.ACID > 0)
                    {
                        SqlCommand cmd1 = new SqlCommand("sp_AgencyContactsDetail_S", conn);
                        cmd1.Parameters.AddWithValue("@IUD", "DetailS");
                        cmd1.Parameters.AddWithValue("@ACID", dass.Tables[0].Rows[0]["ACID"]);
                        cmd1.Parameters.AddWithValue("@IgnoreID", "100");
                        cmd1.CommandType = CommandType.StoredProcedure;
                        conn.Open();

                        DataSet ds = new DataSet();
                        SqlDataAdapter das = new SqlDataAdapter();
                        das.SelectCommand = cmd1;
                        das.Fill(ds);
                        conn.Close();
                        DataRow[] lstDs = ds.Tables[0].Select();
                        foreach (var item in lstDs)
                        {
                            ContactDetailObject obj = new ContactDetailObject();
                            obj.ACID = (int)item["ACID"];
                            obj.WriteDate = Convert.ToDateTime(item["WriteDate"]);
                            obj.IsInquiry = (bool)item["IsInquiry"];
                            obj.Content = item["Content"].ToString();
                            obj.Subject = item["Subject"].ToString();
                            if ((DateTime.Now - (DateTime)item["WriteDate"]).TotalDays < 3.0)
                                obj.IsNew = true;
                            obj.Flag = (bool)item["IsInquiry"] ? "To POLY" : "From POLY";
                            contactDetailObjectList.Add(obj);
                        }


                        /// contactDetailObjectList = (List<ContactDetailObject>) lstDs;

                        //// serviceBase.GetAgencyContactDetail(agencyContactObject2.ACID);
                        if (contactDetailObjectList.Count > 0)
                        {
                            foreach (ContactDetailObject contactDetailObject in contactDetailObjectList)
                            {
                                contactDetailObject.FromOrTo = contactDetailObject.IsInquiry ? "text-to" : "text-from";
                                contactDetailObject.Flag = contactDetailObject.IsInquiry ? "To POLY" : "From POLY";
                                contactDetailObject.Content = contactDetailObject.Content.Trim().Replace("\n", "|");
                                if ((DateTime.Now - contactDetailObject.WriteDate).TotalDays < 3.0)
                                    contactDetailObject.IsNew = true;
                            }
                            this.Session["ContactDetails"] = (object)contactDetailObjectList;
                        }
                        this.Session["agencyContact"] = (object)agencyContactObject2;
                        lstContact.DataSource = contactDetailObjectList;
                        lstContact.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.Message.ToString() + "');", true);
            }



        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = false;
                AgencyContactObject agencyContact = new AgencyContactObject();
                AgentInfoObject agent = (AgentInfoObject)Session["AgentInformation"];
                agencyContact.Subject = Subject.Value;
                agencyContact.Content = Content.Value;
                agencyContact.EmployeeID = agent.EmployeeID;
                if (this.Session["agencyContact"] != null)
                {
                    agencyContact.ACID = ((AgencyContactObject)this.Session["agencyContact"]).ACID;
                    flag = InsertInquiry(agencyContact);
                }
                else
                {
                    agencyContact.AgencyID = agent.AgencyID;
                    agencyContact.AgentID = agent.AgentID;
                    flag = InsertNewContact(agencyContact);
                }
                Response.Redirect("Contact.aspx");
            }
            catch (Exception ex)
            {

                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.Message.ToString() + "');", true);
            }

        }

        private bool InsertNewContact(AgencyContactObject agencyContact)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    SqlCommand storedProcedure = new SqlCommand("sp_AgencyContacts_IUD", conn);
                    storedProcedure.CommandType = CommandType.StoredProcedure;
                    storedProcedure.Parameters.AddWithValue("@IUD", "I");
                    storedProcedure.Parameters.AddWithValue("@AgencyID", agencyContact.AgencyID);
                    storedProcedure.Parameters.AddWithValue("@AgentID", agencyContact.AgentID);
                    storedProcedure.Parameters.AddWithValue("@Subject", agencyContact.Subject);
                    storedProcedure.Parameters.AddWithValue("@Content", agencyContact.Content);
                    storedProcedure.Parameters.AddWithValue("@EmployeeID", agencyContact.EmployeeID);
                    storedProcedure.Connection.Open();
                    storedProcedure.ExecuteScalar();
                    storedProcedure.Connection.Close();
                    return true;
                }
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        private bool InsertInquiry(AgencyContactObject agencyContact)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    SqlCommand storedProcedure = new SqlCommand("sp_AgencyContactsDetails_IUD", conn);
                    storedProcedure.CommandType = CommandType.StoredProcedure;
                    storedProcedure.Parameters.AddWithValue("@IUD", "I");
                    storedProcedure.Parameters.AddWithValue("@ACID", Convert.ToInt32(agencyContact.ACID));
                    storedProcedure.Parameters.AddWithValue("@EmployeeID", agencyContact.EmployeeID);
                    storedProcedure.Parameters.AddWithValue("@Subject", agencyContact.Subject);
                    storedProcedure.Parameters.AddWithValue("@Content", agencyContact.Content);
                    storedProcedure.Parameters.AddWithValue("@IsInquiry", "1");
                    storedProcedure.Connection.Open();
                    storedProcedure.ExecuteScalar();
                    storedProcedure.Connection.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }



    }
}