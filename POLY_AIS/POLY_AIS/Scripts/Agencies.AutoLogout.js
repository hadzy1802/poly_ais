﻿// Create the namespace if it does not exist
if (!window.AgenciesUtils)
    window.AgenciesUtils = {};

/**
* Auto-logout functionality
* @static
* @singleton
* @class AgenciesUtils.autoLogout
*/
AgenciesUtils.autoLogout = function () {
    return {
        nEndTimeout: null,
        nWarningTimeout: null,
        count: null,

        start: function (timeout, warningTime, logoutUrl) {
            var _self = this;
            _self.nWarningTimeout = setTimeout(function () {
                _self.showWarning(timeout, warningTime, logoutUrl);
            }, timeout);
        },

        showWarning: function (timeout, warningTime, logoutUrl) {
            _self = this;
            count = warningTime / 1000;
            var counter = setInterval(_self.timer, 1000);

            $('#sessionTimeout').html(_self.alertText);
            $('#sessionTimeout').slideDown(1000);
            $('body').append("<div id=\"canvasloader-mask\"></div>");
            $('#canvasloader-mask').show();

            $('.timeoutPrompt a').attr('href', logoutUrl);

            $('.timeoutPrompt .stillHere').click(function () {
                // Stop the impending forced logout
                clearTimeout(_self.nEndTimeout);
                _self.nEndTimeout = null;

                // Reset the warning timeout so the user will see it again when the newly-extended session is about to end
                clearTimeout(_self.nWarningTimeout);
                _self.nWarningTimeout = setTimeout(function () {
                    _self.showWarning(timeout, warningTime, logoutUrl);
                }, timeout);

                $('body').find('#canvasloader-mask').remove();
                $('#sessionTimeout').slideUp(1000);
                // Hit the server to get the server-side session timeout extended
                $.ajax('/Account/extend-session', { cache: false });
            });

            // The warning is shown -- if no action within the specified time, force logout
            _self.nEndTimeout = setTimeout(function () {
                window.location = logoutUrl;
            }, warningTime);
        },

        timer: function () {
        	count = count - 1;

        	$('.timeoutPrompt span.time').text(count);
        },

        alertText:
            '<div class="timeoutPrompt container">' +
            '    <div class="col-xs-12">' +
            '       <div class="timeout-icon"><span class="fa fa-exclamation-triangle"></span></div>' +
            '       <div class="timeout-text">' +
            '           <h3>Hello, are you still there?</h3>' +
            '           <p>You haven\'t done anything for a while,<br />so we are logging you out in <span class="time"></span> seconds.</p>' +
            '           <p>Do you want to stay logged in?</p>' +
            '       </div>' +
            '    </div>' +
            '    <div class="col-xs-12">' +
            '       <div class="logoutActions">' +
            '           <a class="btn-xs btn-warning"><span class="fa fa-sign-out"></span> No, Log me out</a>' +
            '           <div class="btn-xs btn-primary stillHere"><span class="fa fa-refresh"></span> Yes, Keep me logged in</div>' +
            '       </div>' +
            '    </div>' +
            '</div>'
    }
} ();
