﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class Documents : System.Web.UI.Page
    {
        private string csHQ = WebConfigurationManager.ConnectionStrings["HQ"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["AgentName"] == null)
                {
                    Response.Redirect("Index.aspx?doLogin=true");
                }
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                 
                    SqlCommand cmd = new SqlCommand("UP_AgencyLiterature_IUD", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IUD", "S");                    
                    conn.Open();

                    DataSet dataSet = new DataSet();// source.RunDataSet();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;

                    da.Fill(dataSet);

                    grdDocs.DataSource = dataSet;
                    grdDocs.DataBind();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}