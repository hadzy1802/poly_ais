﻿using POLY_AIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class ViewMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AgentName"] == null)
            {
                Response.Redirect("Index.aspx?doLogin=true");
            }
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int id = Convert.ToInt32(Request.QueryString["id"]);
                    MessageBoardObject messageBoardObject = new MessageBoardObject();
                    DataSet ds = (DataSet)this.Session["MessageBoardList"];
                    DataTable dt = ds.Tables[0];
                    DataRow[] dr = dt.Select("BoardID = " + id);

                    DataView dv = ds.Tables[0].DefaultView;
                    dv.RowFilter = "BoardID = " + id;
                    // messageBoardObject = dv.Count;  //((Message)).MessageList.Where<MessageBoardObject>((Func<MessageBoardObject, bool>)(m => m.BoardID == id)).FirstOrDefault<MessageBoardObject>();
                    if (dv.Count > 0)
                    {
                        lblBeginDt.Text = dr[0]["PostingBeginDate"].ToString(); //messageBoardObject.PostingBeginDate.ToShortDateString();
                        lblEndDt.Text = dr[0]["PostingEndDate"].ToString(); //messageBoardObject.PostingEndDate.ToShortDateString();
                        lblMessage.Text = dr[0]["Message"].ToString(); //messageBoardObject.Message;
                        lblSubject.Text = dr[0]["Subject"].ToString(); // messageBoardObject.Subject;
                        lblType.Text = dr[0]["BoardType"].ToString(); //messageBoardObject.BoardType;
                    }
                }
            }
        }
    }
}