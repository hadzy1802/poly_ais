﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ApplicationConfirm.aspx.cs" Inherits="POLY_AIS.ApplicationCnfirm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
	<div class="row">
		<div class="col-md-12">
			<div class="featured container">
				<div class="content-wrapper container">
					<h1>AGENCY APPLICATION</h1><br /><br />
					<b>Dear Applicant:</b>
					<br /><br />
					We have received your application. For us to start processing your
					application, please email as attachments the copies of all applicable
					licenses and/or permits to hq@polylanguages.edu.  The license(s) or
					permit(s) can be in the language provided by the governing entities
					applicable to the organization where the business is practiced.
					<br /><br />
					Please contact hq@polylanguages.edu for any questions, and we
					appreciate greatly for your interest in working with POLY Languages Institute.
					<br /><br />
					Sincerely,
					<br />
					POLY Languages Institute
					<br /><br />
				</div>
			</div>

			<section class="main-content form-horizontal">
			</section>
		</div>
	</div>
</div>
</asp:Content>
