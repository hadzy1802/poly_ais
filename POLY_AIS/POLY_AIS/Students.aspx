﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Students.aspx.cs" Inherits="POLY_AIS.Students" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
        <div class="row">
            <div class="col-md-12">
                <div class="featured container">
                    <div class="content-wrapper container no-padding">
                        <div class="col-xs-6">
                            <h1>Student List</h1>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a class="no-spin btn btn-primary" href="javascript:ShowBookWindows(this)">Register New Student</a>
                            <input type="hidden" id="agencyId" name="name" runat="server" value=" " />
                            <input type="hidden" id="agentId" runat="server" name="name" value=" " />

                        </div>
                    </div>
                </div>

                <section class="main-content">
                    <div class="k-widget k-grid" id="StudentsGrid" data-role="grid">
                        <table role="grid" tabindex="0" data-role="selectable" class="k-selectable">
                            <colgroup>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                            </colgroup>
                            <thead class="k-grid-header" role="rowgroup">
                                <tr role="row">
                                    <th class="k-header" data-field="CampusID" data-index="0" data-title="Campus" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Students/StudentList_Read?StudentsGrid-sort=CampusID-asc" tabindex="-1">Campus</a></th>
                                    <th class="k-header" data-field="StudentName" data-index="1" data-title="Name" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Students/StudentList_Read?StudentsGrid-sort=StudentName-asc" tabindex="-1">Name</a></th>
                                    <th class="k-header" data-field="Email" data-index="2" data-title="Email" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Students/StudentList_Read?StudentsGrid-sort=Email-asc" tabindex="-1">Email</a></th>
                                    <th class="k-header" data-field="Status" data-index="3" data-title="Status" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Students/StudentList_Read?StudentsGrid-sort=Status-asc" tabindex="-1">Status</a></th>
                                    <th class="k-header" data-field="DateApplied" data-index="4" data-title="Date Applied" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Students/StudentList_Read?StudentsGrid-sort=DateApplied-asc" tabindex="-1">Date Applied</a></th>
                                    <th class="k-header" data-field="StartDate" data-index="5" data-title="Start Date" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Students/StudentList_Read?StudentsGrid-sort=StartDate-asc" tabindex="-1">Start Date</a></th>
                                    <th class="k-header" data-field="LastDate" data-index="6" data-title="Last Date" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Students/StudentList_Read?StudentsGrid-sort=LastDate-asc" tabindex="-1">Last Date</a></th>
                                </tr>
                            </thead>
                            <tbody role="rowgroup"></tbody>
                        </table>
                        <div class="k-pager-wrap k-grid-pager k-widget k-floatwrap" data-role="pager"><a class="k-link k-pager-nav k-state-disabled k-pager-first" data-page="1" href="#" title="Go to the first page" tabindex="-1"><span class="k-icon k-i-seek-w">seek-w</span></a><a class="k-link k-pager-nav k-state-disabled" data-page="1" href="#" title="Go to the previous page" tabindex="-1"><span class="k-icon k-i-arrow-w">arrow-w</span></a><ul class="k-pager-numbers k-reset">
                            <li class="k-current-page"><span class="k-link k-pager-nav">0</span></li>
                            <li><span class="k-state-selected">0</span></li>
                        </ul>
                            <a class="k-link k-pager-nav k-state-disabled" data-page="0" href="#" title="Go to the next page" tabindex="-1"><span class="k-icon k-i-arrow-e">arrow-e</span></a><a class="k-link k-pager-nav k-state-disabled k-pager-last" data-page="0" href="#" title="Go to the last page" tabindex="-1"><span class="k-icon k-i-seek-e">seek-e</span></a><span class="k-pager-sizes k-label"><span title="" class="k-widget k-dropdown k-header" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false" aria-busy="false" aria-activedescendant="3a25cab1-0055-430c-8979-b97c3149dd6b" style=""><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">20</span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><select data-role="dropdownlist" style="display: none;"><option value="5">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                            </select></span>items per page</span><a class="k-pager-refresh k-link" href="/AIS/Students/StudentList_Read" title="Refresh"><span class="k-icon k-i-refresh">Refresh</span></a><span class="k-pager-info k-label">No items to display</span></div>
                    </div>
                    <script>
                        function ShowBookWindows(element) {
                            var agncy = $(<%= agencyId.ClientID %>).val();
                            var agnt = $(<%= agentId.ClientID %>).val();
                            
                            var url = "http://72.194.200.45/POLY/Admissions/OnlineApp2.aspx?agencyid=" + agncy + "&amp;agentid=" + agnt;
                            OpenWindow(850, 930, url, 0);
                        }
                      //  jQuery(function () { jQuery("#StudentsGrid").kendoGrid({ "columns": [{ "title": "Campus", "headerAttributes": { "data-field": "CampusID", "data-title": "Campus" }, "field": "CampusID", "encoded": true }, { "title": "Name", "headerAttributes": { "data-field": "StudentName", "data-title": "Name" }, "field": "StudentName", "encoded": true }, { "title": "Email", "headerAttributes": { "data-field": "Email", "data-title": "Email" }, "field": "Email", "encoded": true }, { "title": "Status", "headerAttributes": { "data-field": "Status", "data-title": "Status" }, "field": "Status", "encoded": true }, { "title": "Date Applied", "headerAttributes": { "data-field": "DateApplied", "data-title": "Date Applied" }, "field": "DateApplied", "encoded": true }, { "title": "Start Date", "headerAttributes": { "data-field": "StartDate", "data-title": "Start Date" }, "field": "StartDate", "encoded": true }, { "title": "Last Date", "headerAttributes": { "data-field": "LastDate", "data-title": "Last Date" }, "field": "LastDate", "encoded": true }], "pageable": { "refresh": true, "pageSizes": [5, 10, 20], "buttonCount": 10 }, "sortable": { "allowUnsort": false }, "selectable": "Single, Row", "scrollable": false, "messages": { "noRecords": "No records available." }, "dataSource": { "type": (function () { if (kendo.data.transports['aspnetmvc-ajax']) { return 'aspnetmvc-ajax'; } else { throw new Error('The kendo.aspnetmvc.min.js script is not included.'); } })(), "transport": { "read": { "url": "/AIS/Students/StudentList_Read" }, "prefix": "" }, "pageSize": 20, "page": 1, "total": 0, "serverPaging": true, "serverSorting": true, "serverFiltering": true, "serverGrouping": true, "serverAggregates": true, "filter": [], "schema": { "data": "Data", "total": "Total", "errors": "Errors", "model": { "fields": { "StudentID": { "type": "number" }, "CampusID": { "type": "string" }, "EmployeeID": { "type": "number" }, "DateApplied": { "type": "date", "defaultValue": null }, "Status": { "type": "string" }, "LastName": { "type": "string" }, "FirstName": { "type": "string" }, "MiddleName": { "type": "string" }, "StudentName": { "editable": false, "type": "string" }, "DOB": { "type": "date", "defaultValue": null }, "Gender": { "type": "string" }, "Email": { "type": "string" }, "Address": { "type": "string" }, "City": { "type": "string" }, "State": { "type": "string" }, "ZipCode": { "type": "string" }, "Country": { "type": "string" }, "Phone": { "type": "string" }, "Fax": { "type": "string" }, "StartDate": { "type": "date", "defaultValue": null }, "LastDate": { "type": "date", "defaultValue": null }, "CountryOfBirth": { "type": "string" }, "Citizenship": { "type": "string" }, "I20SentDate": { "type": "date", "defaultValue": null }, "I20SentVia": { "type": "string" }, "TrackingNo": { "type": "string" }, "AgencyID": { "type": "string" }, "InvoiceNo": { "type": "number" }, "InvoiceCnt": { "type": "number" }, "Transcript": { "type": "string" }, "Invoice": { "type": "string" }, "FAddress": { "type": "string" }, "FCity": { "type": "string" }, "FCountry": { "type": "string" }, "FFax": { "type": "string" }, "FPhone": { "type": "string" }, "FStateProvince": { "type": "string" }, "FPostalCode": { "type": "string" }, "StudentFiles": { "type": "object" }, "AirportPickup": { "type": "object" }, "HousingHomestay": { "type": "object" }, "HousingDormitory": { "type": "object" } } } } }, "rowTemplate": "\u003ctr\u003e\u003ctd class=\u0027text-center\u0027\u003e#: CampusID #\u003c/td\u003e\u003ctd\u003e\u003ca href=\u0027javascript:RedirectToApplication(this, \"/AIS/Students/StudentDetails/#:StudentID#/#:CampusID#\", \"StudentsGrid\")\u0027 class=\u0027text-primary\u0027\u003e#: StudentName #\u003c/a\u003e\u003c/td\u003e\u003ctd\u003e#: Email #\u003c/td\u003e\u003ctd class=\u0027text-center\u0027\u003e#: Status #\u003c/td\u003e\u003ctd class=\u0027text-center\u0027\u003e#: kendo.toString(DateApplied, \u0027MM/dd/yyyy\u0027) #\u003c/td\u003e\u003ctd class=\u0027text-center\u0027\u003e#: kendo.toString(StartDate, \u0027MM/dd/yyyy\u0027) #\u003c/td\u003e\u003ctd class=\u0027text-center\u0027\u003e#: kendo.toString(LastDate, \u0027MM/dd/yyyy\u0027) #\u003c/td\u003e\u003c/tr\u003e", "navigatable": true }); });
                    </script>
                </section>
            </div>
        </div>
    </div>
</asp:Content>
