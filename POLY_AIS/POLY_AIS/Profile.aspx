﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="POLY_AIS.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/Agencies.Notification.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
        <div class="row">
            <div class="col-md-12">
                <div class="featured container">
                    <div class="content-wrapper container info-container">
                        <h1>My Profile</h1>
                    </div>
                </div>

                <section class="main-content no-padding">
                    <div class="col-xs-12 list-container">
                        <div class="col-xs-12">
                            <h4 class="text-bold text-primary">
                                <span class="fa fa-info-circle"></span>Company Information
                            </h4>
                        </div>
                        <table class="contactdetail info-container" role="tabpanel">
                            <tbody>
                                <tr>
                                    <td width="14%">Company Name</td>
                                    <td>
                                        <asp:Label Text="" ID="lblCompName" runat="server" /></td>
                                    <td width="13%">Country</td>
                                    <td>
                                        <asp:Label Text="" ID="lblCountry" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Website</td>
                                    <td colspan="3">
                                        <asp:Label Text="" ID="lblWebsite" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td><span class="hidden-sm hidden-xs">Date</span> Accepted</td>
                                    <td>
                                        <asp:Label Text="" ID="lblDtAcc" runat="server" /></td>
                                    <td>Contract <span class="hidden-sm hidden-xs">Date</span></td>
                                    <td>
                                        <asp:Label Text="" ID="lblContractDt" runat="server" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group form-field">
                        <p>&nbsp;</p>
                    </div>
                    <div class="col-xs-6">
                        <h4 class="text-bold text-primary">
                            <span class="fa fa-users"></span>Agents Management
                        </h4>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a href="#" onclick='AddOrUpdateAgent(this, "Add", "AddAgent.aspx")' class='btn btn-primary' id="btnLogin">Add Agent</a>
                    </div>
                    <div class="col-xs-12 agentlist-container">


                        <asp:GridView runat="server" ID="grdProfile" AutoGenerateColumns="false" CssClass="table-container">
                            <Columns>
                                <asp:BoundField DataField="CourtesyTitle" HeaderText="" />
                                <asp:TemplateField HeaderText="NAME">
                                    <ItemTemplate>
                                        <%# Eval("FIRSTNAME")%> <%# Eval("LASTNAME")%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="POSITIONTITLE" HeaderText="POSITION" />
                                <asp:BoundField DataField="EMAIL" HeaderText="EMAIL" />
                                <asp:TemplateField HeaderText="PASSWORD">
                                    <ItemTemplate>******</ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="USER TYPE">
                                    <ItemTemplate>                                
                                        <%# Eval("ADMIN").ToString() == "True" ? "<span class='text-red text-bold'>Administrator</span>" : "<span class='text-blue text-bold'>Standard</span>"%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <a href="javascript:void(0);" class="text-link text-primary" id="btnEdtAgnt" onclick="AddOrUpdateAgent(this, 'Add', 'AddAgent.aspx?id=<%# Eval("AgentID")%>')">Edit</a>
                                        <a href="javascript:void(0);" class="text-link text-red" id="btnDelAgnt" style='<%# Eval("IsSelf").ToString() == "True" ? "display:none;": "" %>' onclick='ConfirmationForDeletion(this, "Agent", "<%# Eval("AgencyID")%>", "<%# Eval("AgentID")%>")'>Delete</a>
                                        <a href="javascript:void(0);" class="text-link text-info" id="btnMakeAdmin" onclick="RedirectAdministratorRole(this,'<%# Eval("AgentID") %>','Admin')" style='<%# ( Eval("IsSelf").ToString() == "True" || (Eval("IsSelf").ToString() == "False" && Eval("ADMIN").ToString() == "True"))? "display:none;": "" %>'>Make Admin</a>
                                        <a href="javascript:void(0);" class="text-link text-info" id="btnMakeStd" onclick="RedirectAdministratorRole(this,'<%# Eval("AgentID") %>','Std')" style='<%# ( Eval("IsSelf").ToString() == "True"  || Eval("ADMIN").ToString() == "False") ? "display:none;": "" %>'>Make STD</a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                        <asp:HiddenField ID="hdnAgentId" runat="server" />
                        <asp:HiddenField ID="hdnmkAgentId" runat="server" />
                        <asp:HiddenField ID="hdnMkStdAgntId" runat="server" />
                        <asp:HiddenField ID="hdnAgencyId" runat="server" />
                        <asp:Button Text="makeAdmin" ID="btnMkAdmin" OnClick="btnMkAdmin_Click" runat="server" Style="display: none;" />
                        <asp:Button Text="makeAdmin" ID="btnMkStd" OnClick="btnMkStd_Click" runat="server" Style="display: none;" />
                    </div>
                    <button id="btnOpenModal" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" style="display: none;"></button>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" title="Close">
                                        <img src="/Content/images/icon-close-black.png"></button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body" id="detailsDiv">
                                    <table class="modal-table">
                                        <tbody>
                                            <tr class="noHoverBgColor">
                                                <td>
                                                    <span class="fa fa-exclamation-circle text-red"></span>
                                                </td>
                                                <td class="text-danger" id="modal-message"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer no-padding-right">
                                    <div class="col-xs-9 modal-footer-message text-left">
                                    </div>
                                    <div class="col-xs-3">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button id="btnOpendeleteConfirmModal" type="button" class="btn btn-info" data-toggle="modal" data-target="#deleteConfirmModal" style="display: none;"></button>
                    <div id="deleteConfirmModal" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-deleteconfirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <img src="/Content/images/icon-close-black.png"></button>
                                    <h4 class="modal-title">Delete Confirmation</h4>
                                </div>
                                <div class="modal-body">
                                    <table class="contactdetail">
                                        <tbody>
                                            <tr class="noHoverBgColor">
                                                <td>
                                                    <span class="fa fa-exclamation-circle text-red"></span>
                                                </td>
                                                <td class="text-info">
                                                    <div>
                                                        Are you sure you want to delete this <span id="deletetype">Agent</span>?<br>
                                                        If this was the action that you wanted to do, please confirm your choice, or cancel and return to the page.
                                                    </div>
                                                    <div class="text-right">
                                                        <span onclick="CancelDeleteScenario(this)" class="btn btn-default"><span class="fa fa-undo"></span>Cancel</span>
                                                        <asp:Button Text="Delete Agent" class="btn btn-danger" runat="server" ID="btnAgentDel" OnClick="btnAgentDel_Click" />
                                                        <a id="btnDeleteConfirm" class="btn btn-danger"><span class="fa fa-close"></span>Delete Agent</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        function CancelDeleteScenario(elemnt) {
                            $('.close').trigger('click');
                        }
                    </script>

                </section>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function AddOrUpdateAgent(elemnt, type, url) {
            var src;
            $('#detailsDiv').html("").removeClass("modal-body").css({ "height": "330px" });
            $(".modal-dialog").addClass("viewDetails").css({ "width": "700px" });
            $(".modal-footer").hide();
            src = "<iframe id='myIFrame' scrolling='no' width='100%' height='303px' frameborder='0' src='" + url + "' />";

            if (type === "Update") {
                $('body').find('#canvasloader-mask').remove();
                $('body').find('#canvasloader-container').remove();
                $("#mask").show();

                ShowModalPopup("Update Agent", src);
            }
            else
                ShowModalPopup("Add Agent", src);
        }

        function ConfirmationForDeletion(elemnt, type, agencyID, agentID) {
            if (type === "Agent") {
                debugger;
                $(<%= hdnAgentId.ClientID%>).val(agentID);
                $(<%= hdnAgencyId.ClientID%>).val(agencyID);
            }
            //$('#btnDeleteConfirm').attr("href", "/DeleteAgent" + agencyID + "/" + agentID);

            $('#btnOpendeleteConfirmModal').trigger('click');
        }

        function RedirectAdministratorRole(elemnt, agentID, str) {
            $(<%= hdnmkAgentId.ClientID%>).val(agentID);
            if (str == 'Admin')
                $(<%= btnMkAdmin.ClientID %>).trigger('click');
            else
                $(<%= btnMkStd.ClientID %>).trigger('click');

        }

    </script>
</asp:Content>
