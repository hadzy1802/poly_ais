﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POLY_AIS.App_Code
{
    public class AgencyInfoObject
    {
        public virtual string AgencyID { get; set; }

        
        public virtual string Address { get; set; }

        
        public virtual string City { get; set; }

        
        public virtual string CompanyName { get; set; }

        
        public virtual string Country { get; set; }

        
        public virtual string Fax { get; set; }

        
        public virtual string Tel { get; set; }

        
        public virtual string Province { get; set; }

        
        public virtual string PostalCode { get; set; }

        
        public virtual int Commission { get; set; }

        
        public virtual string WebAddress { get; set; }

        
        public virtual DateTime? DateAccepted { get; set; }

        
        public virtual DateTime? ContractDateFrom { get; set; }

        
        public virtual DateTime? ContractDateTo { get; set; }

        
        public virtual List<AgentInfoObject> Agents { get; set; }
    }

    public class AgencyContactObject
    {
        
        public virtual int ACID { get; set; }

        
        public virtual bool Responded { get; set; }

        
        public virtual string AgencyID { get; set; }

        
        public virtual string AgentID { get; set; }

        
        public virtual int NoInqs { get; set; }

        
        public virtual DateTime InqDate { get; set; }

        
        public virtual int NoReps { get; set; }

        
        public virtual DateTime ResDate { get; set; }

        
        public virtual string CompanyName { get; set; }

        
        public virtual string FirstName { get; set; }

        
        public virtual string LastName { get; set; }

        
        public virtual string CourtesyTitle { get; set; }

        
        public virtual string Email { get; set; }

        
        public virtual string Password { get; set; }

        
        public virtual string Alias { get; set; }

        
        public virtual string IsForDirector { get; set; }

        
        public int EmployeeID { get; set; }

        
        public virtual string Subject { get; set; }

        
        public virtual string Content { get; set; }
    }

    public class MessageBoardObject
    {
       
        public virtual int BoardID { get; set; }

       
        public virtual string BoardType { get; set; }

       
        public virtual string Subject { get; set; }

       
        public virtual string Message { get; set; }

       
        public virtual DateTime PostingBeginDate { get; set; }

       
        public virtual DateTime PostingEndDate { get; set; }

       
        public virtual string Attachment { get; set; }

       
        public virtual string AttachmentPath { get; set; }

        public virtual bool IsNew { get; set; }
    }

    public class Message
    {
        public List<MessageBoardObject> MessageList { get; set; }
    }

    public class ContactDetailObject
    {
        
        public virtual int Num { get; set; }

        
        public virtual int ACID { get; set; }

        
        public virtual int EmployeeID { get; set; }

        
        public DateTime WriteDate { get; set; }

        
        public virtual string Subject { get; set; }

        
        public virtual string Content { get; set; }

        
        public virtual bool IsInquiry { get; set; }

        public virtual string Flag { get; set; }

        public virtual string FromOrTo { get; set; }

        
        public virtual string Alias { get; set; }

        
        public virtual string FirstName { get; set; }

        
        public virtual string LastName { get; set; }

        public virtual bool IsNew { get; set; }
    }
}