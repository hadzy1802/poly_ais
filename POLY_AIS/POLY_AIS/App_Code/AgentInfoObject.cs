﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POLY_AIS.App_Code
{
    public class AgentInfoObject
    {
       
            public virtual string AgencyID { get; set; }

          
            public virtual string AgentID { get; set; }

            public virtual bool IsSelf { get; set; }

            public virtual bool IsAdminUser { get; set; }

          
            public virtual string Email { get; set; }

          
            public virtual string Password { get; set; }

          
            public virtual string FirstName { get; set; }

          
            public virtual string LastName { get; set; }

            public virtual string AgentName
            {
                get
                {
                    return (this.FirstName + " " + this.LastName).Trim();
                }
            }

          
            public virtual int Commission { get; set; }

          
            public virtual string CourtesyTitle { get; set; }

          
            public virtual string PositionTitle { get; set; }

          
            public virtual string CompanyName { get; set; }

          
            public virtual int EmployeeID { get; set; }

          
            public virtual bool Admin { get; set; }

          
            public virtual string AgencyType { get; set; }
        }
    
}