﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ViewMessage.aspx.cs" Inherits="POLY_AIS.ViewMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
	<div class="row">
		<div class="col-md-12">
			<div class="featured container">
				<div class="content-wrapper container">
					<h1>View Message</h1>
				</div>
			</div>

			<section class="main-content form-horizontal info-container view-message no-padding-left">
				<div class="form-group form-field">
					<div class="col-xs-2 text-bold text-primary">
						Posting Begin Date
					</div>
					<div class="col-xs-9">
                        <asp:Label Text="" ID="lblBeginDt" runat="server" />
					</div>
				</div>
				<div class="form-group form-field">
					<div class="col-xs-2 text-bold text-primary">
						Posting End Date
					</div>
					<div class="col-xs-9">
						  <asp:Label Text="" ID="lblEndDt" runat="server" />
					</div>
				</div>
				<div class="form-group form-field">
					<div class="col-xs-2 text-bold text-primary">
						Subject
					</div>
					<div class="col-xs-9">
						 <asp:Label Text="" ID="lblSubject" runat="server" />
					</div>
				</div>
				<div class="form-group form-field">
					<div class="col-xs-2 text-bold text-primary">
						Type
					</div>
					<div class="col-xs-9">
						 <asp:Label Text="" ID="lblType" runat="server" />
					</div>
				</div>
				<div class="form-group form-field">
					<div class="col-xs-2 text-bold text-primary">
						Message
					</div>
					<div class="col-xs-9">
						 <asp:Label Text="" ID="lblMessage" runat="server" />
					</div>
				</div>
				<div class="form-group form-field">
					<div class="col-xs-12 text-center">
						<br>
						<a href="MessageBoard.aspx" class="btn btn-info"><span class="fa fa-arrow-circle-left "></span>Back to List</a>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
</asp:Content>
