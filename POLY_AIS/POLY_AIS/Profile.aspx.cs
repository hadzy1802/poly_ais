﻿using POLY_AIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace POLY_AIS
{
    public partial class Profile : System.Web.UI.Page
    {
        private string csHQ = WebConfigurationManager.ConnectionStrings["HQ"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["AgentName"] == null)
                {
                    Response.Redirect("Index.aspx?doLogin=true");
                }
                AgentInfoObject agent = (AgentInfoObject)Session["AgentInformation"];
               
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    AgencyInfoObject agencyInfoObject = (AgencyInfoObject)null;
                    SqlCommand cmd = new SqlCommand("sp_AgencyAgent_S", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AgencyID", agent.AgencyID);
                    cmd.Parameters.AddWithValue("@AgentID", agent.AgentID);
                    cmd.Parameters.AddWithValue("@IsAdmin",agent.Admin);
                    conn.Open();

                    DataSet dataSet = new DataSet();// source.RunDataSet();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;

                    da.Fill(dataSet);

                    if (dataSet != null)
                    {
                        if (dataSet.Tables.Count > 0)
                        {
                            if (dataSet.Tables[0].Rows.Count > 0)
                            {                                
                                agencyInfoObject = CreateItemFromRow<AgencyInfoObject>(dataSet.Tables[0].Rows[0]);
                                if (dataSet.Tables[1].Rows.Count > 0)
                                    agencyInfoObject.Agents = CreateListFromTable<AgentInfoObject>(dataSet.Tables[1]);
                                foreach (AgentInfoObject agents in agencyInfoObject.Agents)
                                {
                                    agents.AgencyID = agent.AgencyID;
                                    agents.IsSelf = agents.AgentID == agent.AgentID ;
                                    agents.IsAdminUser =agent.Admin;
                                    agents.CourtesyTitle = agent.CourtesyTitle;
                                }
                            }
                        }

                        lblCompName.Text = agencyInfoObject.CompanyName;
                        lblContractDt.Text = agencyInfoObject.ContractDateFrom.ToString();
                        lblCountry.Text = agencyInfoObject.Country;
                        lblDtAcc.Text = agencyInfoObject.DateAccepted.ToString();
                        lblWebsite.Text = agencyInfoObject.WebAddress;
                        Session["AgencyInformation"] = agencyInfoObject;
                        grdProfile.DataSource = agencyInfoObject.Agents;
                        grdProfile.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.ToString() + "');", true);
            }

        }

        public static T CreateItemFromRow<T>(DataRow row) where T : new()
        {
            T obj = new T();
            SetItemFromRow<T>(obj, row);
            return obj;
        }

        public static void SetItemFromRow<T>(T item, DataRow row) where T : new()
        {
            foreach (DataColumn column in (InternalDataCollectionBase)row.Table.Columns)
            {
                PropertyInfo property = item.GetType().GetProperty(column.ColumnName);
                if (property != (PropertyInfo)null && row[column] != DBNull.Value)
                    property.SetValue((object)item, row[column], (object[])null);
            }
        }
        public static List<T> CreateListFromTable<T>(DataTable tbl) where T : new()
        {
            List<T> objList = new List<T>();
            foreach (DataRow row in (InternalDataCollectionBase)tbl.Rows)
                objList.Add(CreateItemFromRow<T>(row));
            return objList;
        }

        protected void btnAgentDel_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(csHQ))
                { 
                    SqlCommand cmd = new SqlCommand("sp_Agent_IUD", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IUD","D" );
                    cmd.Parameters.AddWithValue("@AgencyID", hdnAgencyId.Value   );
                    cmd.Parameters.AddWithValue("@AgentID", hdnAgentId.Value);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    SetNotification("It was successfully deleted.");
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "setTimeout(function(){window.location.href='Profile.aspx'},3000);", true);

                }
            }catch(Exception ex){
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.ToString() + "');", true);
            }
        }

        protected void SetNotification(string notification)
        {
            this.Response.Cookies.Add(new HttpCookie("AgenciesNotification")
            {
                Value = string.Format(notification),
                Path = "/"
            });
        }

        protected void btnMkAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    SqlCommand cmd = new SqlCommand("sp_Agent_IUD", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IUD", "ADMIN");                   
                    cmd.Parameters.AddWithValue("@AgentID", hdnmkAgentId.Value);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    SetNotification("It was successfully processed.");

                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "setTimeout(function(){window.location.href='Profile.aspx'},3000);", true);

                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.ToString() + "');", true);
            }
        }

        protected void btnMkStd_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    SqlCommand cmd = new SqlCommand("sp_Agent_IUD", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IUD", "STD");
                    cmd.Parameters.AddWithValue("@AgentID", hdnmkAgentId.Value);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    SetNotification("It was successfully processed.");
                    Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "setTimeout(function(){window.location.href='Profile.aspx'},3000);", true);

                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.ToString() + "');", true);
            }
        }

       
    }
}