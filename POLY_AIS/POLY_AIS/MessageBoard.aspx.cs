﻿using POLY_AIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class MessageBoard : System.Web.UI.Page
    {
        private string csHQ = WebConfigurationManager.ConnectionStrings["HQ"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["AgentName"] == null)
                {
                    Response.Redirect("Index.aspx?doLogin=true");
                }
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    Message message1 = new Message();

                    DataSet ds = new DataSet();
                    SqlCommand cmd = new SqlCommand("UP_AgencyMessageBoard_IUD", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IUD", "AIS");                    
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;

                    da.Fill(ds);

                    //message1.MessageList = (List<MessageBoardObject>)ds.Tables[0].DefaultView;
                    //foreach (MessageBoardObject message2 in message1.MessageList)
                    //{
                    //    if ((DateTime.Now - message2.PostingBeginDate).TotalDays < 3.0)
                    //        message2.IsNew = true;
                    //}

                    //this.Session["MessageBoardList"] = (object)message1;

                    grdMessage.DataSource = ds; //cmd.ExecuteReader();
                    grdMessage.DataBind();
                    this.Session["MessageBoardList"] = ds;

                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.Message.ToString() + "');", true);
            }
        }

        
    }
}