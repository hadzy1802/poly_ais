﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{

    public partial class BecomeAgency : System.Web.UI.Page
    {
        private string csHQ = WebConfigurationManager.ConnectionStrings["HQ"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
             
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    string password = new Random().Next().ToString();
                    string str = "[split]"; int i = -1;
                    string Questionaire = Questionaire_1.SelectedValue + str + Questionaire2.Value + str + Questionaire3.Value + str + Questionaire4.Value + str + Questionaire5.Value + str + Questionaire6.Value + str + Questionaire7.Value;
                    SqlCommand cmd = new SqlCommand("sp_AgencyApps_I", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CountryCode", CountryCode.SelectedValue );
                    cmd.Parameters.AddWithValue("@CompanyName", CompanyName.Value);
                    cmd.Parameters.AddWithValue("@Address", Address.Value);
                    cmd.Parameters.AddWithValue("@City", City.Value);
                    cmd.Parameters.AddWithValue("@Province", Province.Value);
                    cmd.Parameters.AddWithValue("@PostalCode", PostalCode.Value);
                    cmd.Parameters.AddWithValue("@Country", CountryCode.SelectedValue);
                    cmd.Parameters.AddWithValue("@Tel", Tel.Value);
                    cmd.Parameters.AddWithValue("@Fax", Fax.Value);
                    cmd.Parameters.AddWithValue("@WebAddress", WebAddress.Value);
                    //   cmd.Parameters.AddWithValue("@Note", );
                    cmd.Parameters.AddWithValue("@CourtesyTitle", "");
                    cmd.Parameters.AddWithValue("@FirstName", FirstName.Value);
                    cmd.Parameters.AddWithValue("@LastName", LastName.Value);
                    cmd.Parameters.AddWithValue("@PositionTitle", PositionTitle.Value);
                    cmd.Parameters.AddWithValue("@Email", Email.Value);
                    cmd.Parameters.AddWithValue("@Pwd", password);
                    cmd.Parameters.AddWithValue("@Questionaire", Questionaire);
                    cmd.Connection.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            i = Convert.ToInt32(dr["AgencyAppNumber"].ToString());
                        }
                        cmd.Connection.Close();
                    }
                    if (i < 0)
                    {

                    }
                    else
                    {
                        SqlCommand cmd1 = new SqlCommand("sp_AgencyLocation_IUD", conn);
                        cmd1.CommandType = CommandType.StoredProcedure;
                        if (!string.IsNullOrEmpty(B1CompanyName.Value))
                        {
                            cmd1.Parameters.AddWithValue("@IUD", "A");
                            cmd1.Parameters.AddWithValue("@Num", i);
                            cmd1.Parameters.AddWithValue("@BranchType", "B1");
                            cmd1.Parameters.AddWithValue("@CompanyName", B1CompanyName.Value);
                            cmd1.Parameters.AddWithValue("@Address", B1Address.Value);
                            cmd1.Parameters.AddWithValue("@City", B1City.Value);
                            cmd1.Parameters.AddWithValue("@Province", B1Province.Value);
                            cmd1.Parameters.AddWithValue("@PostalCode", B1PostalCode.Value);
                            cmd1.Parameters.AddWithValue("@Country", B1CountryCode.SelectedItem.Text);
                            cmd1.Parameters.AddWithValue("@CountryCode", B1CountryCode.SelectedValue);
                            cmd1.Connection.Open();
                            cmd1.ExecuteNonQuery();
                            cmd1.Connection.Close();
                        }
                        cmd1 = new SqlCommand("sp_AgencyLocation_IUD", conn);
                        cmd1.CommandType = CommandType.StoredProcedure;
                        if (!string.IsNullOrEmpty(B2CompanyName.Value))
                        {
                            cmd1.Parameters.AddWithValue("@IUD", "A");
                            cmd1.Parameters.AddWithValue("@Num", i);
                            cmd1.Parameters.AddWithValue("@BranchType", "B2");
                            cmd1.Parameters.AddWithValue("@CompanyName", B2CompanyName.Value);
                            cmd1.Parameters.AddWithValue("@Address", B2Address.Value);
                            cmd1.Parameters.AddWithValue("@City", B2City.Value);
                            cmd1.Parameters.AddWithValue("@Province", B2Province.Value);
                            cmd1.Parameters.AddWithValue("@PostalCode", B2PostalCode.Value);
                            cmd1.Parameters.AddWithValue("@Country", B2CountryCode.SelectedItem.Text);
                            cmd1.Parameters.AddWithValue("@CountryCode", B2CountryCode.SelectedValue);
                            cmd1.Connection.Open();
                            cmd1.ExecuteNonQuery(); cmd1.Connection.Close();
                        }
                        cmd1 = new SqlCommand("sp_AgencyLocation_IUD", conn);
                        cmd1.CommandType = CommandType.StoredProcedure;
                        if (!string.IsNullOrEmpty(B3CompanyName.Value))
                        {
                            cmd1.Parameters.AddWithValue("@IUD", "A");
                            cmd1.Parameters.AddWithValue("@Num", i);
                            cmd1.Parameters.AddWithValue("@BranchType", "B3");
                            cmd1.Parameters.AddWithValue("@CompanyName", B3CompanyName.Value);
                            cmd1.Parameters.AddWithValue("@Address", B3Address.Value);
                            cmd1.Parameters.AddWithValue("@City", B3City.Value);
                            cmd1.Parameters.AddWithValue("@Province", B3Province.Value);
                            cmd1.Parameters.AddWithValue("@PostalCode", B3PostalCode.Value);
                            cmd1.Parameters.AddWithValue("@Country", B3CountryCode.SelectedItem.Text);
                            cmd1.Parameters.AddWithValue("@CountryCode", B3CountryCode.SelectedValue);
                            cmd1.Connection.Open();
                            cmd1.ExecuteNonQuery(); cmd1.Connection.Close();
                        }

                        string name = FirstName.Value + " " + LastName.Value;
                        
                        string subject = "Agency Application";
                        string email = Email.Value;
                        using (MailMessage mm = new MailMessage("DoNotReply@polylanguages.edu", email))
                        {
                            mm.Subject = subject;
                            StringBuilder strmsg = new StringBuilder();
                            strmsg.Append("<html><head></head><body>");
                            strmsg.Append("<b>Hi " + name + ", <br>");
                            strmsg.Append("<br><br>");
                            strmsg.Append("<table><tr><td valign='top' style='font-size: 12px;'>");
                            strmsg.Append("<br><font color='red'>*********** DO NOT RESPOND TO THIS E-MAIL ***********</font><br><br>");
                            strmsg.Append("<b>Dear Applicant:</b>                                                                    ");
                            strmsg.Append("<br><br>                                                                                  ");
                            strmsg.Append("We have received your application. For us to start processing your                        ");
                            strmsg.Append("application, please email as attachments the copies of all applicable                     ");
                            strmsg.Append("licenses and/or permits to hq@polylanguages.edu.  The license(s) or                       ");
                            strmsg.Append("permit(s) can be in the language provided by the governing entities                       ");
                            strmsg.Append("applicable to the organization where the business is practiced.                           ");
                            strmsg.Append("<br><br>                                                                                  ");
                            strmsg.Append("Please contact hq@polylanguages.edu for any questions, and we                             ");
                            strmsg.Append("appreciate greatly for your interest in working with POLY Languages Institute.            ");
                            strmsg.Append("<br><br>                                                                                  ");
                            strmsg.Append("Sincerely,                                                                                ");
                            strmsg.Append("<br>                                                                                      ");
                            strmsg.Append("POLY Languages Institute                                                                  ");
                            strmsg.Append("<br><br></td></tr></table>");

                            
                            
                            strmsg.Append("<br><br>");
                            strmsg.Append("</body>");
                            mm.Body = strmsg.ToString();

                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("DoNotReply@polylanguages.edu", "polysmtp2900");
                            smtp.UseDefaultCredentials = true;

                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);

                            ///CodeHelper.ShowMessageBox(this, "Error", "Your Message has been sent. Your Password has been sent. Thank you for your interest in POLY Languages.", "ContactUs.aspx");

                        }
                    }
                    Response.Redirect("ApplicationConfirm.aspx");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}