﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
            //this.Session.RemoveAll();
            //this.Session["AgentInformation"] = null;
            //this.Session["AgentName"] = null;
            Session.Remove("AgentName");
            Response.Redirect("Index.aspx", false);
            //Context.ApplicationInstance.CompleteRequest();
        }
    }
}