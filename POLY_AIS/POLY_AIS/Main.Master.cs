﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["AgentName"] != null)
                {
                    lblname.Text = Session["AgentName"].ToString();
                    signed.Visible = true;
                    notsigned.Visible = false;
                    lblUserNm.Text = "Hi, " + Session["AgentName"].ToString();
                     
                    ulNm.Visible = true;
                }
                else
                {
                    signed.Visible = false;
                    notsigned.Visible = true;                     
                    ulNm.Visible = false;
                   // Response.Redirect("Index.aspx");
                }
            }
        }
    }
}