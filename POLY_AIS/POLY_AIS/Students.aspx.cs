﻿using POLY_AIS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POLY_AIS
{
    public partial class Students : System.Web.UI.Page
    {
        private string csHQ = WebConfigurationManager.ConnectionStrings["HQ"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            //StoredProcedure storedProcedure = new StoredProcedure("sp_AgencyStudents_S", DataService.GetInstance("Agencies"), "dbo");
            //storedProcedure.Command.AddParameter("@AgencyID", (object)agencyID, DbType.String);
            //storedProcedure.Command.AddParameter("@TEST", (object)"TEST", DbType.String);
            try
            {
                if (Session["AgentName"] == null)
                {
                    Response.Redirect("Index.aspx?doLogin=true");
                }
                using (SqlConnection conn = new SqlConnection(csHQ))
                {
                    AgentInfoObject agent = (AgentInfoObject)Session["AgentInformation"];
                    agencyId.Value = agent.AgencyID;
                    agentId.Value = agent.AgentID;
                    //Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + agent.AgencyID  + "');", true);
                    DataSet ds = new DataSet();
                    SqlCommand cmd = new SqlCommand("sp_AgencyStudents_S", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AgencyID", agent.AgencyID);
                    cmd.Parameters.AddWithValue("@TEST", "TEST");
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                   // grdStudent.DataSource = ds; //cmd.ExecuteReader();
                 //   grdStudent.DataBind();

                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", "alert('" + ex.ToString() + "');", true);
            }
        }
    }
}