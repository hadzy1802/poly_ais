﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="POLY_AIS.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="English School in Irvine, Pasadena, and Los Angeles California.">
    <meta name="title" content="English School in Irvine, Pasadena, and Los Angeles California.">
    <meta name="author" content="Poly Languages Institute">
    <meta name="application-name" content="Poly Languages Institute Website">
    <meta name="keywords" content="English School, ESL School, Language School, Irvine, Pasadena, Los Angeles, LA, Orange County, OC, California, USA, TOEFL">
    <meta property="og:title" content="Poly Lanuages Insitute">
    <meta property="og:description" content="English School in Irvine, Pasadena, and Los Angeles California.">
    <meta property="og:url" content="https://www.polylanguages.edu">
    <link rel="shortcut icon" href="/favicon.ico">
    <title>Agency Information System - Poly Lanuages Insitute</title>
    <link href="Content/css/bootstrap.css" rel="stylesheet">
    <link href="Content/css/style.css" rel="stylesheet">

    <link href="Content/css/Agencies.Notification.css" rel="stylesheet">

    <link href="Content/kendo/kendo.common-bootstrap.min.css" rel="stylesheet">
    <link href="Content/kendo/kendo.bootstrap.min.css" rel="stylesheet">

    <script src="Scripts/jquery-2.1.4.js"></script>
    <script src="Scripts/jquery-migrate-1.2.1.min.js"></script>
   <%-- <script src="Scripts/canvasloader.js"></script>
    <script src="Scripts/LoadingProgress.js"></script>--%>
    <script src="Scripts/Agencies.AutoLogout.js"></script>

    <script src="Scripts/Agencies.Notification.js"></script>

  

    <script src="Scripts/modernizr-2.6.2.js"></script>
    <script src="Scripts/modernizr-2.8.3.js"></script>



</head>
<body class="lb-body" marginwidth="0" marginheight="0">
    <div>



        <div class="container-login no-padding-left">
            <h4>APPROVED USER LOGIN</h4>
            <div class="lb-Close">
                <img src="Content/images/icon-close-white.png">
            </div>
            <div class="col-md-12">
                <section id="loginForm">
                    <form  id="form1" class="form-horizontal" runat="server">
                      
                        <div class="form-group form-field">
                            <label class="col-xs-4 control-label text-left" for="Username">Username</label>
                            <div class="col-xs-8">
                                <input runat="server" class="form-control" data-val="true" data-val-issafeparam="The field Username is invalid." data-val-length="The field Username must be a string with a maximum length of 50." data-val-length-max="50" data-val-required="The Username field is required." id="Username" name="Username" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <label class="col-xs-4 control-label" for="Password">Password</label>
                            <div class="col-xs-8">
                                <input runat="server" class="form-control" data-val="true" data-val-required="The Password field is required." id="Password" name="Password" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-8">
                                <a class="ForgotPassword" href="Account/ForgotPassword" target="_top">Forgot password?</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-8">
                                <div class="checkbox">
                                    <input data-val="true" data-val-required="The Remember my username field is required." id="RememberMe" name="RememberMe" type="checkbox" value="true"><input name="RememberMe" type="hidden" value="false">
                                    <label for="RememberMe"><span><span></span></span>Remember Me</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-8 form-errorMessage">
                                &nbsp;
                            </div>
                            <div class="col-xs-4 text-right">
                                <asp:Button Text="Login" CssClass="btn btn-warning" runat="server" ID="loginbtn" OnClick="btnLogin_Click" />
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>

        <script src="Scripts/bootstrap.js"></script>
       
        <script src="/Scripts/jquery.cookie.js"></script>
        <script src="/Scripts/jquery.easing.1.3.js"></script>
        <script src="/Scripts/superfish.js"></script>
        <script src="/Scripts/jquery.rd-navbar.js"></script>
      
        <script src="/Scripts/mailform/jquery.form.min.js"></script>
        <script src="/Scripts/mailform/jquery.rd-mailform.min.js"></script>
        <script src="/Scripts/jquery.rd-parallax.js"></script>

       

        <script>
            function OpenWindow(w, h, url, menubar) {
                var LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
                var TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;

                var newWin = window.open(
                    url,
                    "",
                    "width=" + w + ", " +
                    "height=" + h + ", " +
                    "left=" + LeftPosition + ", " +
                    "top=" + TopPosition + ", " +
                    "menubar=" + menubar + ", resizable=1, scrollbars=1"
                );

                if (!newWin || newWin.closed || typeof newWin.closed === 'undefined') {
                    $(".msg-popupblocked").show();
                    $("html, body").animate({ scrollTop: $(".msg-popupblocked").offset().top - 20 }, 400);
                }
                else
                    newWin.focus();
            }

            function switchText(object, baseText, eventName) {
                if (eventName === "blur") {
                    if (object.value === "") {
                        object.value = baseText;
                    }
                }
                else {
                    if (object.value === baseText) {
                        object.value = "";
                    }
                }
            }

           

            function SetModalDimension(enableMaxHeight) {
                var nWidth = $(window).width() - 200;
                var nHeight = $(window).height() - 125;
                if (!enableMaxHeight && nHeight > 690) nHeight = 690;
                $('#detailsDiv').html("").removeClass("modal-body").css({ "height": nHeight + "px" });
                $(".modal-dialog").addClass("viewDetails").css({ "width": nWidth + "px" });

                return nHeight;
            }

            function ShowModalPopup(title, src) {
                $(".modal-title").html(title);
                $('#detailsDiv').html(src);
                $('#detailsDiv section').removeClass("bg-gradation");
                $('#btnOpenModal').trigger('click');
            }

            function ViewDocument(elemnt, dataType, seqID, fileIndexID, docName) {
                DisplayLoadingSpinner();
                var src;
                var nHeight = SetModalDimension(false);

                $.ajax({
                    url: "Documents/GetDocumentFileName",
                    type: "POST",
                    data: { dataType: dataType, seqID: seqID, fileIndexID: fileIndexID },
                    success: function (data, status, headers) {
                        var isSuccess = (data.isSuccess === true);
                        if (isSuccess) {
                            src = "<iframe id='myIFrame' scrolling='auto' width='100%' height='" + (nHeight - 27) + "px' frameborder='0' src=" + data.FileName + " />";
                            ShowModalPopup("View Document - " + docName, src);
                        }
                        VanishLoadingSpinner();
                    }
                });
            }

            function CloseParentModal(element) {
                window.top.$('button.close').trigger('click');
            }

            function RedirectToApplication(element, redirectUrl, gridName) {
                DisplayLoadingSpinner();
                var grid = $("#" + gridName).data("kendoGrid");
                var dataSource = grid.dataSource;
                var state = {
                    columns: grid.columns,
                    page: dataSource.page(),
                    pageSize: dataSource.pageSize(),
                    sort: dataSource.sort(),
                    filter: dataSource.filter(),
                    group: dataSource.group()
                };

                $.ajax({
                    url: "/Ajax/SaveGridStatus",
                    type: "POST",
                    data: {
                        data: JSON.stringify(state), sessionName: gridName + "Data"
                    }
                });

                window.top.location.assign(redirectUrl);
            }

            function LoadKendoGridFilter(gridName) {
                var grid = $("#" + gridName).data("kendoGrid");
                var dataSource = grid.dataSource;

                $.ajax({
                    url: "/Ajax/LoadGridStatus",
                    type: "POST",
                    data: {
                        sessionName: gridName + "Data"
                    },
                    success: function (state) {
                        state = JSON.parse(state);

                        var options = grid.options;

                        options.columns = state.columns;
                        options.dataSource.page = state.page;
                        options.dataSource.pageSize = state.pageSize;
                        options.dataSource.sort = state.sort;
                        options.dataSource.filter = state.filter;
                        options.dataSource.group = state.group;

                        grid.destroy();

                        $("#" + gridName)
                           .empty()
                           .kendoGrid(options);
                    }
                });
            }

            function ShowBookWindow(element) {
                var url = "";
                OpenWindow(850, 930, url, 0);
            }

            $(document).ready(function () {
              
              
                $(".form-horizontal").find(".label-required").each(function () {
                    $(this).html("<span class='text-required'>*</span> " + $(this).html());
                });
            });
        </script>


    </div>


</body>
</html>
