function switchText(object, baseText, eventName) {
    if (eventName === "blur") {
        if (object.value === "") {
            object.value = baseText;
        }
    }
    else {
        if (object.value === baseText) {
            object.value = "";
        }
    }
}

function GetUserEnvironment() {
    var height = $(window).height();
    var width = $(window).width();

    $.ajax({
        url: "/Ajax/GetUserEnvironment",
        type: "POST",
        data: { width: width, height: height },
        success: function (data, status, headers) {
        }
    });
}

function SetModalDimension(enableMaxHeight) {
    var nWidth = $(window).width() - 200;
    var nHeight = $(window).height() - 125;
    if (!enableMaxHeight && nHeight > 690) nHeight = 690;
    $('#detailsDiv').html("").removeClass("modal-body").css({ "height": nHeight + "px" });
    $(".modal-dialog").addClass("viewDetails").css({ "width": nWidth + "px" });

    return nHeight;
}

function ShowModalPopup(title, src) {
    $(".modal-title").html(title);
    $('#detailsDiv').html(src);
    $('#detailsDiv section').removeClass("bg-gradation");
    $('#btnOpenModal').trigger('click');
}

function ViewDocument(elemnt, dataType, seqID, fileIndexID, docName) {
    DisplayLoadingSpinner();
    var src;
    var nHeight = SetModalDimension(false);

    $.ajax({
        url: "/Documents/GetDocumentFileName",
        type: "POST",
        data: { dataType: dataType, seqID: seqID, fileIndexID: fileIndexID },
        success: function (data, status, headers) {
            var isSuccess = (data.isSuccess === true);
            if (isSuccess) {
                src = "<iframe id='myIFrame' scrolling='auto' width='100%' height='" + (nHeight - 27) + "px' frameborder='0' src=" + data.FileName + " />";
                ShowModalPopup("View Document - " + docName, src);
            }
            VanishLoadingSpinner();
        }
    });
}

function AddOrUpdateAgent(elemnt, type, url) {
    var src;
    $('#detailsDiv').html("").removeClass("modal-body").css({ "height": "390px" });
    $(".modal-dialog").addClass("viewDetails").css({ "width": "700px" });
    $(".modal-footer").hide();
    src = "<iframe id='myIFrame' scrolling='no' width='100%' height='363px' frameborder='0' src='" + url + "' />";
    if (type === "Update")
        ShowModalPopup("Update Agent", src);
    else
        ShowModalPopup("Add Agent", src);
}

function ConfirmationForDeletion(elemnt, type, agencyID, agentID) {
    if (type === "Agent")
        $('#btnDeleteConfirm').attr("href", "/Profile/DeleteAgent/" + agencyID + "/" + agentID);

    $('#btnOpendeleteConfirmModal').trigger('click');
}

function ShowPopupWindow(elemnt, ShowIt) {
    var nWidth = $(window).width() - 200;
    var nHeight = $(window).height() - 125;
    if (nHeight > 1050) nHeight = 1050;
    var newWin = window.open(ShowIt, "PopupWindow", "width=" + nWidth + ",height=" + nHeight + ",screenX=100,left=100,screenY=100,top=100,status=no,toolbar=no,menubar=no,scrollbars=yes,location=no", true);

    if (!newWin || newWin.closed || typeof newWin.closed === 'undefined') {
        $(".msg-popupblocked").show();
        $("html, body").animate({ scrollTop: $(".msg-popupblocked").offset().top - 20 }, 400);
    }
    else
        newWin.focus();
}

function CloseParentModal(element) {
    window.top.$('button.close').trigger('click');
}

function formatCurrency(num) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = 0;
    var sign = (num === (num = Math.abs(num)));
    if (num === 0) sign = true;
    num = Math.floor(num * 100 + 0.50000000001);
    var cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + '$' + num + '.' + cents);
}

function formatNumber(pnumber, decimals) {
    if (isNaN(pnumber)) { return 0; }
    if (pnumber === '') { return 0; }

    var snum = new String(pnumber);
    var sec = snum.split('.');
    var whole = parseFloat(sec[0]);
    var result = '';

    if (sec.length > 1) {
        var dec = new String(sec[1]);
        dec = String(parseFloat(sec[1]) / Math.pow(10, (dec.length - decimals)));
        dec = String(whole + Math.round(parseFloat(dec)) / Math.pow(10, decimals));
        var dot = dec.indexOf('.');
        if (dot == -1) {
            dec += '.';
            dot = dec.indexOf('.');
        }
        while (dec.length <= dot + decimals) { dec += '0'; }
        result = dec;
    } else {
        var dot;
        var dec = new String(whole);
        dec += '.';
        dot = dec.indexOf('.');
        while (dec.length <= dot + decimals) { dec += '0'; }
        result = dec;
    }
    return result;
}

function fieldformat_initialize() {
    var field_value;
    $(".form-horizontal").find(".text-currency").each(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
        if ($(this).val().length > 0)
            this.value = '$' + parseFloat(this.value, 20).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    });

    $(".form-horizontal").find(".text-decimal").each(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
        if ($(this).val().length > 0)
            this.value = parseFloat(this.value, 20).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    });

    $(".form-horizontal").find(".text-decimal3").each(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
        if ($(this).val().length > 0)
            this.value = parseFloat(this.value, 20).toFixed(3).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    });
}

$(document).ready(function () {
    GetUserEnvironment();
    fieldformat_initialize();

    /* Manually add dashes as users enters a Phone Number */
    $('.text-phone').keyup(function () {
        if ($(this).val().length > 3 && $(this).val().charAt(3) != '-') {
            $(this).val($(this).val().substr(0, 3) + '-' + $(this).val().substr(3, $(this).val().length));
        }
        if ($(this).val().length > 7 && $(this).val().charAt(7) != '-') {
            $(this).val($(this).val().substr(0, 7) + '-' + $(this).val().substr(7, 4));
        }
        if ($(this).val().length > 12) {
            $(this).val($(this).val().substr(0, 12));
        }
    });

    /* Manually add dashes as users enters a SSN */
    $('.text-SSN').keyup(function () {
        if ($(this).val().length > 3 && $(this).val().charAt(3) != '-') {
            $(this).val($(this).val().substr(0, 3) + '-' + $(this).val().substr(3, $(this).val().length));
        }
        if ($(this).val().length > 6 && $(this).val().charAt(6) != '-') {
            $(this).val($(this).val().substr(0, 6) + '-' + $(this).val().substr(6, 4));
        }
        if ($(this).val().length > 11) {
            $(this).val($(this).val().substr(0, 11));
        }
    });

    /* Manually add dashes as users enters a Zip Code */
    $('#PropertyZipCode').keyup(function () {
        if ($(this).val().length > 5 && $(this).val().charAt(5) != '-') {
            $(this).val($(this).val().substr(0, 5) + '-' + $(this).val().substr(5, $(this).val().length));
        }
    });

    /* Allow numeric only */
    $('.numericonly').keyup(function () {
        this.value = this.value.replace(/[^0-9\-]/g, '');
    });

    /* Manually add dollar sign as users enters a Currency */
    $('.text-currency').blur(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
        if ($(this).val().length > 0)
            this.value = '$' + parseFloat(this.value, 20).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    });

    $('.text-decimal').blur(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
        if ($(this).val().length > 0)
            this.value = parseFloat(this.value, 20).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    });

    /* Global solution to limit the number of characters a user can enter in a field */
    $(".limitInputLength").keyup(function (event) {
        var maxCharNum = parseInt($(this).attr('data-val-length-max'));
        if ($(this).val().length > maxCharNum) {
            $(this).val($(this).val().substring(0, maxCharNum));
        }
    });

    $(".form-horizontal").find(".label-required").each(function () {
        $(this).html("<span class='text-required'>*</span> " + $(this).html());
    });

    $(".form-horizontal .form-top, .form-horizontal .form-top2, .form-horizontal .form-middle2, .form-horizontal .addcolon, .datetracking-containerm, #detailsDiv").find(".control-label").each(function () {
        $(this).html($(this).html() + " :");
    });
});
