﻿$(document).ready(function () {
    $("a:not([target='_blank']):not([data-toggle='tab']):not([data-toggle='collapse']):not('.k-state-disabled'):not('.k-grid-filter'):not('.no-spin'):not('.k-link'):not('.inactive-link')").click(function () {
        DisplayLoadingSpinner();
    });

    $("#SecurityProfile, .borrower-info #LoanID").change(function () {
        var href = window.location.href.toLowerCase();
        if (href.indexOf("pipeline") >= 0)
            DisplayLoadingSpinner();
    });
});

document.onreadystatechange = function () {
    var string = document.readyState;
    if (string === "complete")
        VanishLoadingSpinner();
    else
        DisplayLoadingSpinner();
}

var DisplayLoadingSpinner = function () {
    var zIdx = 2000;
    var nDiameter = 40;
    var posLeft = ($(window).width() / 2 + $(document).scrollLeft() - nDiameter / 2);
    var posTop = ($(window).height() / 2 + $(document).scrollTop() - nDiameter / 2);
    if (posTop > 350) posTop = 350;
    if ($('body').find('#canvasloader-container').length) {
        $('body').find('#canvasloader-mask').remove();
        $('body').find('#canvasloader-container').remove();
    }
    $('body').append("<div id=\"canvasloader-mask\"></div>");
    $('body').append("<div id=\"canvasloader-container\"></div>");
    $("#canvasloader-mask").css({ "z-index": zIdx + 1, "display": "block" });
    $("#canvasloader-container").css({ "z-index": zIdx + 2, "position": "fixed", "top": posTop + "px", "left": posLeft + "px" });

    var cl = new CanvasLoader('canvasloader-container');
    cl.setColor("#2a307a"); // default is '#000000'
    cl.setShape('spiral'); // default is 'oval'
    cl.setDiameter(50); // default is 40
    cl.setDensity(12); // default is 40
    cl.setRange(1.2); // default is 1.3
    cl.setSpeed(1); // default is 2
    cl.setFPS(7); // default is 24
    cl.show(); // Hidden by default
}

var VanishLoadingSpinner = function () {
    $('body').find('#canvasloader-mask').remove();
    $('body').find('#canvasloader-container').remove();
}
