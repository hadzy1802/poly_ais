﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="POLY_AIS.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
    <div class="row">
        <div class="col-md-12">
            <div class="featured container">
                <div class="content-wrapper container">
                    <p>
                        Agency Information System (AIS) is a web-based information system developed for agencies.
                        To access AIS, the user must have a valid email address as the user ID and its corresponding password entered into AIS.
                        By logging on to AIS, the agent can perform or view the following:
                    </p>
                </div>
            </div>

            <section class="main-content">
                <table class="table-container">
                    <tbody><tr>
                        <th>Menu</th>
                        <th>Description</th>
                    </tr>
                    <tr>
                        <td>
                            Contact POLY
                        </td>
                        <td>
                            If you contact us through this medium in English, you can expect a prompt response, within 12 hours of your inquiry - except Saturdays,
                            Sundays, and Holidays
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Students
                        </td>
                        <td>
                            The agent can view the list of students booked. For each student, the agent has access to the student’s personal profile,
                            invoice, tracking number of the sent acceptance packet, flight/housing information of requested services, transcript, etc.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Document &amp; Forms
                        </td>
                        <td>
                            The school policies, procedures, and commission rates for agencies are accessible.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Agency Profile
                        </td>
                        <td>
                            The agent can view his/her company profile and view/edit its users. In order to manage its users of AIS, the agent must be ADMIN of AIS.
                        </td>
                    </tr>
                </tbody></table>
            </section>
        </div>
    </div>
</div>
</asp:Content>
