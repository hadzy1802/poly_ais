﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="POLY_AIS.Documents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
        <div class="row">
            <div class="col-md-12">
                <div class="featured container">
                    <div class="content-wrapper container">
                        <h1>Documents and Forms</h1>
                    </div>
                </div>

                <section class="main-content">
                    <div class="agentlist-container">
                        <asp:GridView runat="server" ID="grdDocs" AutoGenerateColumns="false" CssClass="table-container">
                            <Columns>
                                <asp:BoundField DataField="DocNo" HeaderText="#" />
                                <asp:BoundField DataField="DocType" HeaderText="DOC TYPE" />
                                <asp:TemplateField HeaderText="DOC NAME">
                                    <ItemTemplate>
                                        <a href="<%# System.Configuration.ConfigurationManager.AppSettings["FORMS_PATH"]+"/"+ Eval("FileName") %>" target="_blank"><%# Eval("DocName") %></a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Note" HeaderText="Note" />
                                <asp:BoundField DataField="RevisedDate" HeaderText="DATE"   DataFormatString="{0:MM-dd-yyyy}"/>
                                
                            </Columns>
                            

                        </asp:GridView>
                        <div class="k-pager-wrap k-grid-pager k-widget k-floatwrap" data-role="pager">
                            <a class="k-link k-pager-nav k-state-disabled k-pager-first" data-page="1" href="#" title="Go to the first page" tabindex="-1"><span class="k-icon k-i-seek-w">seek-w</span></a><a class="k-link k-pager-nav k-state-disabled" data-page="1" href="#" title="Go to the previous page" tabindex="-1"><span class="k-icon k-i-arrow-w">arrow-w</span></a><ul class="k-pager-numbers k-reset">
                                <li class="k-current-page"><span class="k-link k-pager-nav">1</span></li>
                                <li><span class="k-state-selected">1</span></li>
                            </ul>
                            <a class="k-link k-pager-nav k-state-disabled" data-page="1" href="#" title="Go to the next page" tabindex="-1"><span class="k-icon k-i-arrow-e">arrow-e</span></a><a class="k-link k-pager-nav k-state-disabled k-pager-last" data-page="1" href="#" title="Go to the last page" tabindex="-1"><span class="k-icon k-i-seek-e">seek-e</span></a><span class="k-pager-sizes k-label"><span title="" class="k-widget k-dropdown k-header" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false" aria-busy="false" aria-activedescendant="2dba7219-7fa7-4411-9b6d-b5cd8ef711a5" style=""><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">20</span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><select data-role="dropdownlist" style="display: none;"><option value="5">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                            </select></span>items per page</span><a class="k-pager-refresh k-link" href="/AIS/Documents/DocumentList_Read" title="Refresh"><span class="k-icon k-i-refresh">Refresh</span></a><span class="k-pager-info k-label">1 - 13 of 13 items</span>
                        </div>
                    </div>
                    <%-- <div class="k-widget k-grid" id="DocumentsAndFormsGrid" data-role="grid">
                        <table role="grid" tabindex="0" data-role="selectable" class="k-selectable">
                            <colgroup>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                            </colgroup>
                            <thead class="k-grid-header" role="rowgroup">
                                <tr role="row">
                                    <th class="k-header" data-field="DocNo" data-index="0" data-title="#" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Documents/DocumentList_Read?DocumentsAndFormsGrid-sort=DocNo-asc" tabindex="-1">#</a></th>
                                    <th class="k-header" data-field="DocType" data-index="1" data-title="Doc Type" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Documents/DocumentList_Read?DocumentsAndFormsGrid-sort=DocType-asc" tabindex="-1">Doc Type</a></th>
                                    <th class="k-header" data-field="DocName" data-index="2" data-title="Doc Name" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Documents/DocumentList_Read?DocumentsAndFormsGrid-sort=DocName-asc" tabindex="-1">Doc Name</a></th>
                                    <th class="k-header" data-field="Note" data-index="3" data-title="Note" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Documents/DocumentList_Read?DocumentsAndFormsGrid-sort=Note-asc" tabindex="-1">Note</a></th>
                                    <th class="k-header" data-field="RevisedDate" data-index="4" data-title="Date" scope="col" data-role="columnsorter"><a class="k-link" href="/AIS/Documents/DocumentList_Read?DocumentsAndFormsGrid-sort=RevisedDate-asc" tabindex="-1">Date</a></th>
                                </tr>
                            </thead>
                            <tbody role="rowgroup">
                                <tr>
                                    <td class="text-center">10</td>
                                    <td class="text-nowrap">Admissions</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Application_for_Admission_2016.pdf" target="_blank" class="text-primary">Application for Admission 2016</a></td>
                                    <td>For F-1 students, it must accompany copy of passport and financial statement.</td>
                                    <td class="text-center">05/18/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">12</td>
                                    <td class="text-nowrap">Admissions</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Catalog_2016.pdf" target="_blank" class="text-primary">Catalog 2016</a></td>
                                    <td></td>
                                    <td class="text-center">05/18/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">14</td>
                                    <td class="text-nowrap">Admissions</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Financial_Support_Gurantee_1601.pdf" target="_blank" class="text-primary">Financial Support Gurantee 1601</a></td>
                                    <td>Required if the financial statement is NOT under student's name.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">16</td>
                                    <td class="text-nowrap">Admissions</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/SEVIS_I-901_Fee_Payment_1506.pdf" target="_blank" class="text-primary">SEVIS I-901 Fee Payment 1506</a></td>
                                    <td>Required if student is applying F-1 visa for the first time.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">20</td>
                                    <td class="text-nowrap">Airport Transfer Service</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Airport_Transfer_Request_1601.pdf" target="_blank" class="text-primary">Airport Transfer Request 1601</a></td>
                                    <td>Optional Airport Transfer Service.  Must complete for the service ASAP.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">22</td>
                                    <td class="text-nowrap">Airport Transfer Service</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Airport_Transfer_Confirmation_1505.pdf" target="_blank" class="text-primary">Airport Transfer Confirmation 1505</a></td>
                                    <td>POLY will send this form to student to confirm the arrival schedule.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">30</td>
                                    <td class="text-nowrap">Housing Service</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Dorm_Lease_Agreement_1603.pdf" target="_blank" class="text-primary">Dorm Lease Agreement 1603</a></td>
                                    <td>Must be completed before checking in.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">32</td>
                                    <td class="text-nowrap">Housing Service</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Dorm_Check-In_Form_1603.pdf" target="_blank" class="text-primary">Dorm Check-In Form 1603</a></td>
                                    <td>POLY will send this form to student to confirm the check-in schedule.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">80</td>
                                    <td class="text-nowrap">Agency</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/POLY_Logo_Usage_Guidelines_1503.pdf" target="_blank" class="text-primary">POLY Logo Usage Guidelines 1503</a></td>
                                    <td></td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">82</td>
                                    <td class="text-nowrap">Agency</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/POLY_Logo_for_Print.tif" target="_blank" class="text-primary">POLY Logo for Print</a></td>
                                    <td>POLY logo for Print purposes.  Review the POLY Logo Usage Guidelines.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">84</td>
                                    <td class="text-nowrap">Agency</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/POLY_Logo_for_Web.jpg" target="_blank" class="text-primary">POLY Logo for Web</a></td>
                                    <td>POLY logo for web purposes.  Review the POLY Logo Usage Guidelines.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">90</td>
                                    <td class="text-nowrap">Agency</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Agency_Guidelines_1501.pdf" target="_blank" class="text-primary">Agency Guidelines 1501</a></td>
                                    <td>General guidelines including commission rates.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                                <tr>
                                    <td class="text-center">92</td>
                                    <td class="text-nowrap">Agency</td>
                                    <td class="text-nowrap"><a href="/POLYSTARS/Forms/AgencyLiterature/Wire_Request_for_Agency_1503.pdf" target="_blank" class="text-primary">Wire Request for Agency 1503</a></td>
                                    <td>Applicable only for countries that do not honor US checks.</td>
                                    <td class="text-center">05/21/2016</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="k-pager-wrap k-grid-pager k-widget k-floatwrap" data-role="pager"><a class="k-link k-pager-nav k-state-disabled k-pager-first" data-page="1" href="#" title="Go to the first page" tabindex="-1"><span class="k-icon k-i-seek-w">seek-w</span></a><a class="k-link k-pager-nav k-state-disabled" data-page="1" href="#" title="Go to the previous page" tabindex="-1"><span class="k-icon k-i-arrow-w">arrow-w</span></a><ul class="k-pager-numbers k-reset">
                            <li class="k-current-page"><span class="k-link k-pager-nav">1</span></li>
                            <li><span class="k-state-selected">1</span></li>
                        </ul>
                            <a class="k-link k-pager-nav k-state-disabled" data-page="1" href="#" title="Go to the next page" tabindex="-1"><span class="k-icon k-i-arrow-e">arrow-e</span></a><a class="k-link k-pager-nav k-state-disabled k-pager-last" data-page="1" href="#" title="Go to the last page" tabindex="-1"><span class="k-icon k-i-seek-e">seek-e</span></a><span class="k-pager-sizes k-label"><span title="" class="k-widget k-dropdown k-header" unselectable="on" role="listbox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-owns="" aria-disabled="false" aria-readonly="false" aria-busy="false" aria-activedescendant="2dba7219-7fa7-4411-9b6d-b5cd8ef711a5" style=""><span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">20</span><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span><select data-role="dropdownlist" style="display: none;"><option value="5">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                            </select></span>items per page</span><a class="k-pager-refresh k-link" href="/AIS/Documents/DocumentList_Read" title="Refresh"><span class="k-icon k-i-refresh">Refresh</span></a><span class="k-pager-info k-label">1 - 13 of 13 items</span></div>
                    </div>
                    <script>
                        jQuery(function () { jQuery("#DocumentsAndFormsGrid").kendoGrid({ "columns": [{ "title": "#", "headerAttributes": { "data-field": "DocNo", "data-title": "#" }, "field": "DocNo", "encoded": true }, { "title": "Doc Type", "headerAttributes": { "data-field": "DocType", "data-title": "Doc Type" }, "field": "DocType", "encoded": true }, { "title": "Doc Name", "headerAttributes": { "data-field": "DocName", "data-title": "Doc Name" }, "field": "DocName", "encoded": true }, { "title": "Note", "headerAttributes": { "data-field": "Note", "data-title": "Note" }, "field": "Note", "encoded": true }, { "title": "Date", "headerAttributes": { "data-field": "RevisedDate", "data-title": "Date" }, "field": "RevisedDate", "encoded": true }], "pageable": { "refresh": true, "pageSizes": [5, 10, 20], "buttonCount": 10 }, "sortable": { "allowUnsort": false }, "selectable": "Single, Row", "scrollable": false, "messages": { "noRecords": "No records available." }, "dataSource": { "type": (function () { if (kendo.data.transports['aspnetmvc-ajax']) { return 'aspnetmvc-ajax'; } else { throw new Error('The kendo.aspnetmvc.min.js script is not included.'); } })(), "transport": { "read": { "url": "/AIS/Documents/DocumentList_Read" }, "prefix": "" }, "pageSize": 20, "page": 1, "total": 0, "serverPaging": true, "serverSorting": true, "serverFiltering": true, "serverGrouping": true, "serverAggregates": true, "filter": [], "schema": { "data": "Data", "total": "Total", "errors": "Errors", "model": { "fields": { "Idx": { "type": "number" }, "DocNo": { "type": "number" }, "DocType": { "type": "string" }, "DocName": { "type": "string" }, "FileName": { "type": "string" }, "Path": { "type": "string" }, "Note": { "type": "string" }, "RevisedDate": { "type": "date" }, "IsNew": { "type": "boolean" } } } } }, "rowTemplate": "\u003ctr\u003e\u003ctd class=\u0027text-center\u0027\u003e#: DocNo #\u003c/td\u003e\u003ctd class=\u0027text-nowrap\u0027\u003e#: DocType #\u003c/td\u003e\u003ctd class=\u0027text-nowrap\u0027\u003e\u003ca href=\u0027/POLYSTARS/Forms/AgencyLiterature/#: FileName #\u0027 target=\u0027_blank\u0027 class=\u0027text-primary\u0027\u003e#: DocName #\u003c/a\u003e#if (IsNew) { #\u003cspan class=\u0027wr-icon wr-new\u0027\u003eN\u003c/span\u003e # } #\u003c/td\u003e\u003ctd\u003e#: Note #\u003c/td\u003e\u003ctd class=\u0027text-center\u0027\u003e#: kendo.toString(RevisedDate, \u0027MM/dd/yyyy\u0027) #\u003c/td\u003e\u003c/tr\u003e", "navigatable": true }); });
                    </script>--%>
                </section>
            </div>
        </div>
    </div>
</asp:Content>
