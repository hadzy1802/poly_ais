﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAgent.aspx.cs" Inherits="POLY_AIS.AddAgent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="English School in Irvine, Pasadena, and Los Angeles California.">
    <meta name="title" content="English School in Irvine, Pasadena, and Los Angeles California.">
    <meta name="author" content="Poly Languages Institute">
    <meta name="application-name" content="Poly Languages Institute Website">
    <meta name="keywords" content="English School, ESL School, Language School, Irvine, Pasadena, Los Angeles, LA, Orange County, OC, California, USA, TOEFL">
    <meta property="og:title" content="Poly Lanuages Insitute">
    <meta property="og:description" content="English School in Irvine, Pasadena, and Los Angeles California.">
    <meta property="og:url" content="https://www.polylanguages.edu">
    <link rel="shortcut icon" href="/favicon.ico">
    <title>Agency Information System - Poly Lanuages Insitute</title>
    <link href="Content/css/bootstrap.css" rel="stylesheet">
    <link href="Content/css/style.css" rel="stylesheet">

    <link href="Content/css/Agencies.Notification.css" rel="stylesheet">

    <link href="Content/kendo/kendo.common-bootstrap.min.css" rel="stylesheet">
    <link href="Content/kendo/kendo.bootstrap.min.css" rel="stylesheet">

    <script src="Scripts/jquery-2.1.4.js"></script>
    <script src="Scripts/jquery-migrate-1.2.1.min.js"></script>
   <%-- <script src="Scripts/canvasloader.js"></script>
    <script src="Scripts/LoadingProgress.js"></script>--%>
    <script src="Scripts/Agencies.AutoLogout.js"></script>
    <script src="Scripts/Agencies.Notification.js"></script>

  

    <script src="Scripts/modernizr-2.6.2.js"></script>
    <script src="Scripts/modernizr-2.8.3.js"></script>


</head>
<body>
    <form id="form1" runat="server">
        <div class="container contents">
            <section class="form-horizontal" id="loginForm">

                <div class="form-group form-field">
                    <label class="col-xs-3 control-label label-required text-primary"  for="CourtesyTitle" runat="server"><span class="text-required">*</span> Title</label>
                    <div class="col-xs-2">
                        <asp:DropDownList runat="server" ID="ddlTitle" >
                            <asp:ListItem Text="Dr." Value="Dr." />
                            <asp:ListItem Text="Miss" Value="Miss" />
                             <asp:ListItem Text="Mr." Value="Mr." />
                             <asp:ListItem Text="Mrs." Value="Mrs." />
                             <asp:ListItem Text="Ms." Value="Ms." />
                        </asp:DropDownList>                         
                    </div>
                </div>

                <div class="form-group form-field">
                    <label class="col-xs-3 control-label label-required text-primary" for="FirstName"><span class="text-required">*</span> First Name</label>
                    <div class="col-xs-8">
                        <input  runat="server" class="form-control" data-val="true" data-val-length="The field First Name must be a string with a maximum length of 20." data-val-length-max="20" data-val-required="The First Name field is required." id="FirstName" name="FirstName" type="text" value="">
                    </div>
                </div>
                <div class="form-group form-field">
                    <label class="col-xs-3 control-label label-required text-primary" for="LastName"><span class="text-required">*</span> Last Name</label>
                    <div class="col-xs-8">
                        <input runat="server"  class="form-control" data-val="true" data-val-length="The field Last Name must be a string with a maximum length of 20." data-val-length-max="20" data-val-required="The Last Name field is required." id="LastName" name="LastName" type="text" value="">
                    </div>
                </div>
                <div class="form-group form-field">
                    <label class="col-xs-3 control-label text-primary" for="PositionTitle">Position</label>
                    <div class="col-xs-6">
                        <input  runat="server" class="form-control" data-val="true" data-val-length="The field Position must be a string with a maximum length of 20." data-val-length-max="20" data-val-required="The Position field is required." id="PositionTitle" name="PositionTitle" type="text" value="">
                    </div>
                </div>
                <div class="form-group form-field">
                    <label class="col-xs-3 control-label text-primary" for="Email">Email</label>
                    <div class="col-xs-7">
                        <input runat="server"  class="form-control" data-val="true" data-val-length="The field Email must be a string with a maximum length of 50." data-val-length-max="50" data-val-regex="That doesn't look like an email." data-val-regex-pattern="^([\w-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" data-val-required="The Email field is required." id="Email" name="Email" type="text" value="">
                    </div>
                </div>
                <div class="form-group form-field">
                    <label class="col-xs-3 control-label text-primary" for="Password">Password</label>
                    <div class="col-xs-3">
                        <input runat="server" class="form-control" id="Password2" name="Password2" type="password"  runat="server">
                        <input id="AgencyID" name="AgencyID" type="hidden" value="" runat="server">
                        <input id="AgentID" name="AgentID" type="hidden" value=""  runat="server">
                        <input  runat="server" data-val="true" data-val-length="The field Password must be a string with a maximum length of 12." data-val-length-max="12" data-val-required="The Password field is required." id="Password" name="Password" type="hidden" value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-5 text-red">
                    </div>
                    <div class="col-xs-6 btn-group">
                        <asp:Button Text="Add" ID="btnAdd" runat="server" OnClick="btnAdd_Click"  class="btn btn-primary btn-submit"/>
                       <%-- <button type="button" class="btn btn-primary btn-submit">
                            <span class="fa fa-save"></span>Add
                        </button>--%>
                        <button type="button" class="btn btn-default" onclick="CloseParentModal(this)">
                            <span class="fa fa-close"></span>Cancel
                        </button>
                    </div>
                </div>

            </section>
        </div>
        
       <%--    <script src="Scripts/bootstrap.js"></script>--%>
       
        <script src="/Scripts/jquery.cookie.js"></script>
        <script src="/Scripts/jquery.easing.1.3.js"></script>
        <script src="/Scripts/superfish.js"></script>
        <script src="/Scripts/jquery.rd-navbar.js"></script>
      
        <script src="/Scripts/mailform/jquery.form.min.js"></script>
        <script src="/Scripts/mailform/jquery.rd-mailform.min.js"></script>
        <script src="/Scripts/jquery.rd-parallax.js"></script>

        <script src="Scripts/jquery.validate.js"></script>
        <script src="Scripts/jquery.validate.unobtrusive.js"></script>
        <script>
            function ShowModalPopup(title, src) {
                debugger;
                $(".modal-title").html(title);
                $('#detailsDiv').html(src);
                $('#detailsDiv section').removeClass("bg-gradation");
                $('#btnOpenModal').trigger('click');
            }
            debugger;
            window.top.$('body').find('#canvasloader-mask').remove();
            window.top.$('body').find('#canvasloader-container').remove();
            window.top.$("#mask").show();


            function CloseParentModal(element) {
                window.top.$('button.close').trigger('click');
            }
        </script>
    </form>
</body>
</html>
