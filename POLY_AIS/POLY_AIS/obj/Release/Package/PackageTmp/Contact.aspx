﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" EnableEventValidation="false" Inherits="POLY_AIS.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
        <div class="row">
            <div class="col-md-12">
                <div class="featured container">
                    <div class="content-wrapper container">
                        <h1>Contact POLY</h1>
                        <p>
                            For any inquiries, e-mail to
						<a href="mailto:hq@polylanguages.edu" class="addr_email">hq@polylanguages.edu</a> or submit this form. If you have any attachments, please e-mail.
                        </p>
                    </div>
                </div>

                <section class="main-content form-horizontal">
                    <form action="/AIS/Home/Contact" class="no-padding-left" id="contactForm" method="post" role="form" novalidate="novalidate">
                        <input name="__RequestVerificationToken" type="hidden" value="_qSzl3Z4MMkxIXSeYi59vbxqAmuJmEe4tEWgIlmfU5Ul8wmQpa4USmaKr6qFluEN_fTG1jxUqLsTcELfKs_dJAXFFv-N0sPQ1xYTvTaWTRosQ1-XbtGqbACgHciBWHDL0">
                        <div class="form-group form-field">
                            <label class="col-xs-2 control-label text-primary" for="Subject">Subject</label>
                            <div class="col-xs-9">
                                <input class="form-control" runat="server" data-val="true" data-val-length="The field Subject must be a string with a maximum length of 150." data-val-length-max="150" data-val-required="The Subject field is required." id="Subject" name="Subject" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group form-field">
                            <label class="col-xs-2 control-label text-primary" for="Content">Content</label>
                            <div class="col-xs-9">
                                <textarea class="form-control" runat="server" cols="20" data-val="true" data-val-required="The Content field is required." id="Content" name="Content" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-8 text-red">
                            </div>
                            <div class="col-xs-11 text-right">
                                <asp:Button CssClass="btn btn-primary" Text="Send" OnClick="btnSend_Click" ID="btnSend" runat="server" />
                                <%--<button type="submit" class="btn btn-primary" runat="server" >
                                    <span class="fa fa-send"></span>Send
                                </button>--%>
                            </div>
                        </div>
                    </form>
                    <div id="ContactDetails">
                        <div id="ContactDetailListView" data-role="listview" class="k-widget k-listview" role="listbox">

                            <asp:ListView runat="server" ID="lstContact">
                                <ItemTemplate>
                                 
                                    <table class="contactdetail" data-uid="f7136ab4-9b5e-4fea-9cc5-28adbd505acd" role="option" aria-selected="false">
                                        <tbody>
                                            <tr class="form-group form-field">
                                                <td class="col-sm-6 contact-headline">
                                                    <h4 class="text-<%# Eval("IsInquiry").ToString() == "True" ? "to":"from" %>"><%# Eval("Flag") %></h4>
                                                </td>
                                                <td class="col-sm-6 text-right text-bold contact-headline">
                                                   <%# Eval("writeDate") %> 
                                                </td>
                                            </tr>
                                            <tr class="form-group form-field">
                                                <td colspan=" 2" class="col-sm-12 contact-subject">
                                                    <strong>Subject : </strong><%# Eval("Subject") %> 
                                                    <span class="wr-icon wr-new"><%# Eval("IsNew").ToString() == "True" ? "N" : "" %></span>
                                                </td>
                                            </tr>
                                            <tr class="form-group form-field">
                                                <td colspan="2" class="col-sm-12 text-content"><%# Eval("Content") %>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>                                    
                                </ItemTemplate>                               
                            </asp:ListView>


                        </div>
                        <div class="k-pager-wrap k-widget k-floatwrap" id="ContactDetailListView_pager" data-role="pager">
                            <a href="#" title="Go to the first page" class="k-link k-pager-nav k-pager-first k-state-disabled" data-page="1" tabindex="-1"><span class="k-icon k-i-seek-w">Go to the first page</span></a><a href="#" title="Go to the previous page" class="k-link k-pager-nav  k-state-disabled" data-page="1" tabindex="-1"><span class="k-icon k-i-arrow-w">Go to the previous page</span></a><ul class="k-pager-numbers k-reset">
                                <li class="k-current-page"><span class="k-link k-pager-nav">1</span></li>
                                <li><span class="k-state-selected">1</span></li>
                            </ul>
                            <a href="#" title="Go to the next page" class="k-link k-pager-nav  k-state-disabled" data-page="1" tabindex="-1"><span class="k-icon k-i-arrow-e">Go to the next page</span></a><a href="#" title="Go to the last page" class="k-link k-pager-nav k-pager-last k-state-disabled" data-page="1" tabindex="-1"><span class="k-icon k-i-seek-e">Go to the last page</span></a><span class="k-pager-info k-label">1 - 1 of 1 items</span>
                        </div>
                        <script>
                            // jQuery(function () { jQuery("#ContactDetailListView").kendoListView({ "dataBound": onDataBound, "dataSource": { "type": (function () { if (kendo.data.transports['aspnetmvc-ajax']) { return 'aspnetmvc-ajax'; } else { throw new Error('The kendo.aspnetmvc.min.js script is not included.'); } })(), "transport": { "read": { "url": "" }, "prefix": "" }, "pageSize": 10, "page": 1, "total": 1, "schema": { "data": "Data", "total": "Total", "errors": "Errors", "model": { "fields": { "Num": { "type": "number" }, "ACID": { "type": "number" }, "EmployeeID": { "type": "number" }, "WriteDate": { "type": "date" }, "Subject": { "type": "string" }, "Content": { "type": "string" }, "IsInquiry": { "type": "boolean" }, "Flag": { "type": "string" }, "FromOrTo": { "type": "string" }, "Alias": { "type": "string" }, "FirstName": { "type": "string" }, "LastName": { "type": "string" }, "IsNew": { "type": "boolean" } } } }, "data": { "Data": [{ "Num": 27221, "ACID": 1236, "EmployeeID": 1, "WriteDate": "\/Date(1473759750500)\/", "Subject": "t", "Content": "t", "IsInquiry": true, "Alias": "JonathanP", "FirstName": "Jonathan", "LastName": "Park", "Flag": "To POLY", "FromOrTo": "text-to", "IsNew": true }], "Total": 1, "AggregateResults": null } }, "template": kendo.template(jQuery('#templateContactDetail').html()), "pageable": { "autoBind": false, "buttonCount": 10, "pagerId": "ContactDetailListView_pager" } }); });
                        </script>
                    </div>
                </section>
            </div>
        </div>
    </div>
</asp:Content>
