﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="MessageBoard.aspx.cs" Inherits="POLY_AIS.MessageBoard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container contents">
	<div class="row">
		<div class="col-md-12">
			<div class="featured container">
				<div class="content-wrapper container">
					<h1>Message Board</h1>
				</div>
			</div>

			<section class="main-content form-horizontal no-padding-left">
                <asp:GridView runat="server" ID="grdMessage" AutoGenerateColumns="false" CssClass="table-container">
                    <Columns>
                        <asp:BoundField DataField="POSTINGBEGINDATE" HeaderText="POSTING BEGIN DATE" DataFormatString="MM/dd/yyyy" />
                        <asp:BoundField DataField="POSTINGENDDATE" HeaderText="POSTING END DATE" DataFormatString="MM/dd/yyyy"/>
                        <asp:TemplateField >
                            <ItemTemplate>
                                <a href="ViewMessage.aspx?id=<%# Eval("BOARDID") %>"><%# Eval("SUBJECT") %></a>
                            </ItemTemplate>
                        </asp:TemplateField> 
                    </Columns>
                </asp:GridView>
					<%--<table class="table-container" role="tabpanel">
						<tbody><tr>
							<th width="19%" class="text-left">Posting Begin Date</th>
							<th width="19%" class="text-left">Posting End Date</th>
							<th width="62%" class="text-left">Subject</th>
						</tr>
							<tr>
								<td>09/08/2016</td>
								<td>09/17/2016</td>
								<td>
									<a href="/AIS/Home/ViewMessage/4" class="text-primary">hello</a>
								</td>
							</tr>
					</tbody></table>--%>
			</section>
		</div>
	</div>
</div>
</asp:Content>
